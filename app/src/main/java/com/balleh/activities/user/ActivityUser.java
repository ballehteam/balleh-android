package com.balleh.activities.user;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.balleh.R;
import com.balleh.additional.AppLanguageSupport;
import com.balleh.additional.ApplicationContext;
import com.balleh.additional.ConstantDataParser;
import com.balleh.additional.ConstantDataSaver;
import com.balleh.additional.ConstantFields;
import com.balleh.additional.instentTransfer.CustomerResponse;
import com.balleh.additional.instentTransfer.User;
import com.balleh.model.ModelAccountDetail;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ActivityUser extends AppCompatActivity implements User, CustomerResponse {

    private ProgressBar pb_UserLoader;
    private String loginFlag = "Default";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        Toolbar toolbar = findViewById(R.id.tb_main_bar);
        setSupportActionBar(toolbar);

        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        pb_UserLoader = findViewById(R.id.pb_user_loader);

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            loginFlag = bundle.getString("WhatToLoad");

            if (loginFlag != null)
                switch (loginFlag) {
                    case "Login":
                        //loadSignIn();
                        break;
                    case "Register":
                       // loadSignUp();
                        break;
                    default:
                       // loadSignIn();
                        break;
                }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStack();
        } else {
            finish();
        }
    }

   /* @Override
    public void SignIn(final JSONObject LoginDetail) {
        try {
            if (LoginDetail != null) {
                pb_UserLoader.setVisibility(View.VISIBLE);
                StringRequest stringRequest = new StringRequest(Request.Method.POST, ConstantFields.mLogin,
                        response -> {
                            pb_UserLoader.setVisibility(View.GONE);
                            Log.e("onResponse(Token): ", response);

                            if (response != null) {

                                ConstantDataSaver.mStoreSharedPreferenceString(ActivityUser.this.getApplicationContext(),
                                        ConstantFields.mTokenTag, response.replace("\"", ""));

                                loadAccountInformation();

                                setCartId();

                                showToast(getString(R.string.user_login_success));
                                finish();
                            } else {
                                showToast(getString(R.string.user_error_invalid_user_detail));
                            }
                        }, error -> {
                            pb_UserLoader.setVisibility(View.GONE);
                            try {
                                if (error.networkResponse != null) {
                                    String responseBody = new String(error.networkResponse.data, "utf-8");
                                    JSONObject jsonObject = new JSONObject(responseBody);
                                    showToast(jsonObject.getString("message"));
                                    Log.e("onErrorResponse: ", jsonObject.toString());
                                } else {
                                    showToast(getString(R.string.user_error_invalid_user_detail));
                                }
                            } catch (Exception e) {
                                showToast(getString(R.string.user_error_invalid_user_detail));
                            }
                        }) {
                    @Override
                    public byte[] getBody() {
                        return LoginDetail.toString().getBytes();
                    }

                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> params = new HashMap<>();
                        params.put("Content-Type", "application/json; charset=utf-8");
                        return params;
                    }
                };

                stringRequest.setShouldCache(false);
                ApplicationContext.getInstance().addToRequestQueue(stringRequest, "SignIn");
            }

        } catch (Exception e) {
            pb_UserLoader.setVisibility(View.GONE);
            showToast(getString(R.string.common_error));
        }

    }

    @Override
    public void SignUp(final JSONObject SignUpDetail, final JSONObject LoginDetail) {
        try {
            if (SignUpDetail != null) {
                pb_UserLoader.setVisibility(View.VISIBLE);

                Log.e("SignUp: ", SignUpDetail.toString());

                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, ConstantFields.mRegistration, SignUpDetail,
                        response -> SignIn(LoginDetail), error -> {
                            pb_UserLoader.setVisibility(View.GONE);
                            try {
                                if (error.networkResponse != null) {
                                    String responseBody = new String(error.networkResponse.data, "utf-8");
                                    JSONObject jsonObject = new JSONObject(responseBody);
                                    showToast(jsonObject.getString("message"));
                                } else {
                                    showToast(getString(R.string.user_error_invalid_user_detail));
                                }
                            } catch (Exception e) {
                                showToast(getString(R.string.user_error_invalid_user_detail));
                            }
                        }) {

                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> params = new HashMap<>();
                        params.put("Content-Type", "application/json; charset=utf-8");
                        return params;
                    }
                };

                jsonObjectRequest.setShouldCache(false);
                ApplicationContext.getInstance().addToRequestQueue(jsonObjectRequest, "SignUp");
            }

        } catch (Exception e) {
            pb_UserLoader.setVisibility(View.GONE);
            showToast(getString(R.string.common_error));
        }
    }*/

    @Override
    public void ForgetPassword(final JSONObject ForgetPasswordDetail) {
        try {
            if (ForgetPasswordDetail != null) {
                pb_UserLoader.setVisibility(View.VISIBLE);
                StringRequest stringRequest = new StringRequest(Request.Method.PUT, ConstantFields.mForgotPassword,
                        response -> {
                            pb_UserLoader.setVisibility(View.GONE);
                            showToast(response);
                        }, error -> {
                            pb_UserLoader.setVisibility(View.GONE);
                            try {
                                if (error.networkResponse != null) {
                                    String responseBody = new String(error.networkResponse.data, "utf-8");
                                    JSONObject jsonObject = new JSONObject(responseBody);
                                    showToast(jsonObject.getString("message"));
                                } else {
                                    showToast(getString(R.string.user_error_invalid_user_detail));
                                }
                            } catch (Exception e) {
                                showToast(getString(R.string.user_error_invalid_user_detail));
                            }
                        }) {
                    @Override
                    public byte[] getBody() {
                        return ForgetPasswordDetail.toString().getBytes();
                    }

                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> params = new HashMap<>();
                        params.put("Content-Type", "application/json; charset=utf-8");
                        return params;
                    }
                };

                stringRequest.setShouldCache(false);
                ApplicationContext.getInstance().addToRequestQueue(stringRequest, "ForgetPassword");
            }

        } catch (Exception e) {
            pb_UserLoader.setVisibility(View.GONE);
            showToast(getString(R.string.common_error));
        }
    }

   /* @Override
    public void loadSignIn() {
        FragmentUserLogin fragmentUserLogin = new FragmentUserLogin();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.ll_user_layout_container, fragmentUserLogin, "SignIn");
        fragmentTransaction.addToBackStack("SignIn");
        fragmentTransaction.commit();
    }

    @Override
    public void loadSignUp() {
        FragmentUserRegistration fragmentUserRegistration = new FragmentUserRegistration();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.ll_user_layout_container, fragmentUserRegistration, "SignUp");
        fragmentTransaction.addToBackStack("SignUp");
        fragmentTransaction.commit();
    }

    @Override
    public void loadForgetPassword() {
        DialogFragmentForgotPassword dialogFragmentForgotPassword = new DialogFragmentForgotPassword();
        dialogFragmentForgotPassword.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        dialogFragmentForgotPassword.setCancelable(true);
        dialogFragmentForgotPassword.show(getSupportFragmentManager(), "ForgotPassword");
    }

    @Override
    public void loadSignInFromSignUp() {
        if (loginFlag.equals("Login")) {
            onBackPressed();
        } else {
            FragmentUserLogin fragmentUserLogin = new FragmentUserLogin();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.ll_user_layout_container, fragmentUserLogin, "SignIn");
            fragmentTransaction.addToBackStack("SignIn");
            fragmentTransaction.commit();
        }
    }*/

    private void showToast(String message) {
        ConstantFields.CustomToast(message, ActivityUser.this);
    }

    private void setCartId() {

        if (!ConstantDataSaver.mRetrieveSharedPreferenceString(ActivityUser.this.getApplicationContext(), ConstantFields.mTokenTag).equals("Empty")) {
            StringRequest stringRequest = new StringRequest(Request.Method.GET, ConstantFields.getCustomerCartId(ConstantFields.currentLanguage().equals("en")),
                    response -> {
                        Log.e("onResponse: ", response);
                        ConstantDataSaver.mStoreSharedPreferenceString(ActivityUser.this.getApplicationContext(),
                                ConstantFields.mCustomerCartIdTag, String.valueOf(ConstantDataParser.getCartId(response)));

                    }, error -> {
                        if (error.networkResponse.statusCode == 401) {
                            ConstantDataSaver.mRemoveSharedPreferenceString(ActivityUser.this.getApplicationContext(), ConstantFields.mTokenTag);
                            ConstantDataSaver.mRemoveSharedPreferenceString(ActivityUser.this.getApplicationContext(), ConstantFields.mCustomerCartIdTag);
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<>();
                    params.put("Content-Type", "application/json; charset=utf-8");
                    params.put("Authorization", "Bearer " + ConstantDataSaver.mRetrieveSharedPreferenceString(ActivityUser.this.getApplicationContext(),
                            ConstantFields.mTokenTag));
                    return params;
                }
            };

            stringRequest.setShouldCache(false);
            ApplicationContext.getInstance().addToRequestQueue(stringRequest, "CartId");
        }
    }

    @Override
    public void getResult(String response, String source) {
        if (response != null) {
            if (!response.equals("Error")) {
                if (source.equals("CartIdUser")) {
                    Log.e("getResult: ", response);
                }
            }
        }
    }

    private void loadAccountInformation() {
        if (!ConstantDataSaver.mRetrieveSharedPreferenceString(ActivityUser.this.getApplicationContext(), ConstantFields.mTokenTag).equals("Empty")) {

            StringRequest stringRequest = new StringRequest(Request.Method.GET, ConstantFields.getCustomerDetail(ConstantFields.currentLanguage().equals("en")),
                    response -> {
                        Log.e("onResponse: account", response + "");
                        ModelAccountDetail modelAccountDetail = ConstantDataParser.getAccountDetail(response);

                        if (modelAccountDetail != null) {
                            if (modelAccountDetail.getEmailId() != null) {
                                ConstantDataSaver.mStoreSharedPreferenceString(ActivityUser.this.getApplicationContext(),
                                        ConstantFields.mCustomerEmailIdTag, modelAccountDetail.getEmailId());

                                ConstantDataSaver.mStoreSharedPreferenceString(ActivityUser.this.getApplicationContext(),
                                        ConstantFields.mCustomerAccountTag, response);

                            }
                        }
                    }, error -> {
                        if (error.networkResponse != null) {
                            if (error.networkResponse.statusCode == 401) {
                                ConstantDataSaver.mRemoveSharedPreferenceString(ActivityUser.this.getApplicationContext(), ConstantFields.mTokenTag);
                            }
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<>();
                    params.put("Content-Type", "application/json");
                    params.put("Authorization", "Bearer "
                            + ConstantDataSaver.mRetrieveSharedPreferenceString(ActivityUser.this.getApplicationContext(),
                            ConstantFields.mTokenTag));
                    return params;
                }
            };

            stringRequest.setShouldCache(false);
            ApplicationContext.getInstance().addToRequestQueue(stringRequest, "CustomerAccountInformation");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Pass the activity result to the fragment, which will then pass the result to the login
        // button.
        Fragment fragment = getFragmentManager().findFragmentById(R.id.ll_user_layout_container);
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(AppLanguageSupport.onAttach(base));
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(
                    "ar".equals(AppLanguageSupport.getLanguage(ActivityUser.this)) ?
                            View.LAYOUT_DIRECTION_RTL : View.LAYOUT_DIRECTION_LTR);
        }
    }

}
