package com.balleh.activities.user;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.balleh.R;
import com.balleh.additional.AppLanguageSupport;
import com.balleh.additional.ApplicationContext;
import com.balleh.additional.ConstantDataParser;
import com.balleh.additional.ConstantDataSaver;
import com.balleh.additional.ConstantFields;
import com.balleh.additional.instentTransfer.User;
import com.balleh.fragments.user.DialogFragmentForgotPassword;
import com.balleh.model.ModelAccountDetail;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ActivityLogin extends AppCompatActivity implements View.OnClickListener, User {

    private EditText et_Email, et_Password;
    private ProgressBar pb_UserLoader;
    private String from = "Empty";

    /*Social Login*/
    /*Twitter*/
    private TwitterLoginButton mTwitterLoginButton;
    /*Facebook*/
    CallbackManager callbackManager;
    LoginButton loginButton;
    String first_name, last_name, user_id, email_id = null, type, mType = null;
    private boolean fromSocialLogin = false;
    Button facebook_custom_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(ActivityLogin.this);
        callbackManager = CallbackManager.Factory.create();
        AppEventsLogger.activateApp(ActivityLogin.this);
        Twitter.initialize(this);
        setContentView(R.layout.activity_login);


        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            if (bundle.getString("From") != null) {
                from = bundle.getString("From");
            }
        }

        Toolbar toolbar = findViewById(R.id.tb_main_bar);
        setSupportActionBar(toolbar);

        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        pb_UserLoader = findViewById(R.id.pb_user_loader);
        load();
    }

    @Override
    public void onBackPressed() {
        if (from.equals("Empty")) {
            super.onBackPressed();
        } else {
            startActivity(new Intent(ActivityLogin.this, ActivitySignUp.class));
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void load() {
        Button btn_SignIn = findViewById(R.id.btn_sign_in);
        Button btn_SignUp = findViewById(R.id.btn_sign_up);
        AppCompatTextView atv_ForgetPassword = findViewById(R.id.atv_forget_password);
        CheckBox cb_ShowPwd = findViewById(R.id.cb_show_pwd_sign_in);
        et_Email = findViewById(R.id.et_email_login);
        et_Password = findViewById(R.id.et_password_login);
        facebook_custom_button = findViewById(R.id.facebook_login_custom);

        social_login_setup();

        btn_SignIn.setOnClickListener(view -> {

            if (et_Email.getEditableText().toString().length() > 0
                    && et_Password.getEditableText().toString().length() > 0
                    && et_Password.getEditableText().toString().length() > 7
                    && ConstantFields.isEmailValidator(et_Email.getEditableText().toString())) {

                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("username", et_Email.getEditableText().toString());
                    jsonObject.put("password", et_Password.getEditableText().toString());
                    SignIn(jsonObject);
                } catch (Exception e) {
                    showToast(getString(R.string.common_error));
                }
            }

            if (et_Email.getText().toString().isEmpty()) {
                showToast(getString(R.string.user_error_email));
            } else {
                if (!ConstantFields.isEmailValidator(et_Email.getText().toString())) {
                    showToast(getString(R.string.user_error_invalid_email));
                } else {
                    if (et_Password.getText().toString().isEmpty()) {
                        showToast(getString(R.string.user_error_password));
                    } else {
                        if (et_Password.getText().toString().length() < 8) {
                            showToast(getString(R.string.user_error_password_length));
                        }
                    }
                }
            }
        });

        cb_ShowPwd.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            if (!isChecked) {
                et_Password.setTransformationMethod(PasswordTransformationMethod.getInstance());
            } else {
                et_Password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            }
        });

        btn_SignUp.setOnClickListener(this);
        atv_ForgetPassword.setOnClickListener(this);

    }

    private void social_login_setup() {
        /*Social login*/

        /*Twitter*/
        mTwitterLoginButton = findViewById(R.id.btn_twitter_social_login);

        mTwitterLoginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                Log.e("success: ", result.data.getUserId() + "");

                first_name = result.data.getUserName();
                last_name = result.data.getUserName();
                mType = "Twitter";
                type = "Twitter";

                TwitterAuthClient authClient = new TwitterAuthClient();
                authClient.requestEmail(result.data, new Callback<String>() {
                    @Override
                    public void success(Result<String> resultEmail) {
                        // Do something with the result, which provides the email address
                        Log.e("success: ", resultEmail.data + "");

                        if (result.data.getUserId() != 0 && resultEmail.data != null) {
                            social_login(String.valueOf(result.data.getUserId()), resultEmail.data, "Twitter");
                        } else if (result.data.getUserId() != 0 && resultEmail.data == null) {
                            social_login(String.valueOf(result.data.getUserId()), "", "Twitter");
                        } else {
                            showToast(getString(R.string.common_error));
                        }
                    }

                    @Override
                    public void failure(TwitterException exception) {
                        // Do something on failure
                        if (result.data.getUserId() != 0) {
                            social_login(String.valueOf(result.data.getUserId()), "", "Twitter");
                        } else {
                            showToast(getString(R.string.common_error));
                        }

                    }
                });

            }

            @Override
            public void failure(TwitterException exception) {
                Log.e("failure: ", exception.getMessage() + "");
                showToast(getString(R.string.common_error));
            }
        });

        /*Twitter*/

        /*Facebook*/
        loginButton = findViewById(R.id.login_button);
        List<String> permissionList = new ArrayList<>();
        permissionList.add("public_profile");
        permissionList.add("email");
        loginButton.setReadPermissions(permissionList);
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {

                Bundle parameters = new Bundle();
                parameters.putString("fields", "email,first_name,last_name");

                GraphRequest requestEmail = new GraphRequest(loginResult.getAccessToken(), "me", parameters, null, new GraphRequest.Callback() {
                    @Override
                    public void onCompleted(GraphResponse response) {
                        if (response != null) {
                            GraphRequest.GraphJSONObjectCallback callbackEmail = (json, response1) -> {
                                try {
                                    user_id = loginResult.getAccessToken().getUserId();

                                    mType = "Facebook";
                                    if (response1.getError() == null) {

                                        first_name = json.getString("first_name");
                                        last_name = json.getString("last_name");

                                        if (json.optString("email") != null && !json.optString("email").equals("")) {
                                            email_id = json.optString("email");
                                            social_login(loginResult.getAccessToken().getUserId(), email_id, "Facebook");
                                        } else {
                                            email_id = null;
                                            social_login(loginResult.getAccessToken().getUserId(), "", "Facebook");
                                        }
                                        type = "Facebook";
                                    }
                                } catch (Exception e) {
                                    showToast(e.toString());
                                }
                            };
                            callbackEmail.onCompleted(response.getJSONObject(), response);
                        }
                    }
                });
                requestEmail.executeAsync();
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {
                showToast(error.getMessage());
            }
        });

        facebook_custom_button.setOnClickListener(v -> LoginManager.getInstance().logInWithReadPermissions(ActivityLogin.this, Arrays.asList("public_profile", "email")));

        /*Facebook*/

        /*Social login*/
    }

    private void social_login(String userId, String email_id, String type) {
        facebook_logout();

        try {
            JSONObject jsonObject = new JSONObject();
            if (type.equals("Facebook")) {
                jsonObject.put("socialType", "Facebook");
            } else {
                jsonObject.put("socialType", "Twitter");
            }
            if (email_id != null) {
                if (email_id.length() > 0) {
                    jsonObject.put("email", email_id);
                } else {
                    jsonObject.put("email", "");
                }
            } else {
                jsonObject.put("email", "");
            }

            jsonObject.put("socialId", userId);
            jsonObject.put("firstname", first_name);
            jsonObject.put("lastname", last_name);
            pb_UserLoader.setVisibility(View.VISIBLE);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, ConstantFields.CustomerSocialLogin,
                    response -> {
                        pb_UserLoader.setVisibility(View.GONE);
                        try {
                            JSONArray jsonArrayResult = new JSONArray(response);
                            JSONObject jsonObjectResult = jsonArrayResult.getJSONObject(0);

                            if (!jsonObjectResult.isNull("customer_token")) {
                                ConstantDataSaver.mStoreSharedPreferenceString(ActivityLogin.this.getApplicationContext(),
                                        ConstantFields.mTokenTag, jsonObjectResult.getString("customer_token"));

                                if (!jsonObjectResult.isNull("mail_required")) {
                                    if (jsonObjectResult.getInt("mail_required") == 1) {
                                        try {
                                            if (!jsonObjectResult.isNull("email")) {
                                                JSONObject jsonForgotPassword = new JSONObject();
                                                jsonForgotPassword.put("email", jsonObjectResult.getString("email"));
                                                jsonForgotPassword.put("template", "email_reset");
                                                jsonForgotPassword.put("websiteId", "1");
                                                fromSocialLogin = true;
                                                ForgetPassword(jsonForgotPassword);
                                            }

                                        } catch (Exception e) {
                                            showFailedMessage();
                                            finish();
                                        }
                                    } else {
                                        successLogin();
                                    }
                                } else {
                                    successLogin();
                                }

                            }
                        } catch (Exception e) {
                            showFailedMessage();
                            finish();
                        }
                    },
                    error -> {
                        pb_UserLoader.setVisibility(View.GONE);
                        try {

                            if (error.networkResponse != null) {
                                String responseBody = new String(error.networkResponse.data, "utf-8");
                                JSONObject jsonObject2 = new JSONObject(responseBody);
                                Log.e("social_login: ", jsonObject2.getString("message") + "");

                                String result = jsonObject2.getString("message");

                                if (result.equals("Not a valid Email")) {
                                    if (email_id == null) {
                                        dialog(getString(R.string.user_login_social_email_error));
                                    } else {
                                        if (email_id.length() <= 0) {
                                            dialog(getString(R.string.user_login_social_email_error));
                                        }
                                    }
                                } else {
                                    showFailedMessage();
                                    finish();
                                }

                            } else {
                                showFailedMessage();
                            }
                        } catch (Exception e) {
                            showFailedMessage();
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<>();
                    params.put("Content-Type", "application/json; charset=utf-8");
                    return params;
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    return jsonObject.toString().getBytes();
                }
            };

            stringRequest.setShouldCache(false);
            ApplicationContext.getInstance().addToRequestQueue(stringRequest, "SocialLogin");

        } catch (Exception e) {
            pb_UserLoader.setVisibility(View.GONE);
            showFailedMessage();
        }

    }

    private void dialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityLogin.this);
        builder.setMessage(message).setCancelable(true);
        builder.setPositiveButton(R.string._continue, (dialog, id) -> dialog.dismiss());
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void successLogin() {
        loadAccountInformation();
        showToast(getString(R.string.user_login_success));
        finish();
    }

    private void showFailedMessage() {
        showToast(getString(R.string.user_error_invalid_user_detail));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == TwitterAuthConfig.DEFAULT_AUTH_REQUEST_CODE) {
            mTwitterLoginButton.onActivityResult(requestCode, resultCode, data);
            /*final TwitterAuthClient twitterAuthClient = new TwitterAuthClient();
            if(twitterAuthClient.getRequestCode()==requestCode) {
                twitterAuthClient.onActivityResult(requestCode, resultCode, data);
            }*/
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.btn_sign_up:
                Intent toSignUp = new Intent(ActivityLogin.this, ActivitySignUp.class);
                toSignUp.putExtra("From", "Login");
                startActivity(toSignUp);
                finish();
                break;
            case R.id.atv_forget_password:
                DialogFragmentForgotPassword dialogFragmentForgotPassword = new DialogFragmentForgotPassword();
                dialogFragmentForgotPassword.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
                dialogFragmentForgotPassword.setCancelable(true);
                dialogFragmentForgotPassword.show(getSupportFragmentManager(), "ForgotPassword");
                break;
        }
    }

    public void SignIn(final JSONObject LoginDetail) {
        try {
            if (LoginDetail != null) {
                pb_UserLoader.setVisibility(View.VISIBLE);

                Log.e( "SignIn: ",LoginDetail.toString()+"" );
                Log.e( "SignIn: ", ConstantFields.mLogin);

                StringRequest stringRequest = new StringRequest(Request.Method.POST, ConstantFields.mLogin,
                        response -> {
                            pb_UserLoader.setVisibility(View.GONE);
                            Log.e("onResponse(Token): ", response);
                            if (response != null) {
                                ConstantDataSaver.mStoreSharedPreferenceString(ActivityLogin.this.getApplicationContext(),
                                        ConstantFields.mTokenTag, response.replace("\"", ""));
                                loadAccountInformation();
                                showToast(getString(R.string.user_login_success));
                            } else {
                                showToast(getString(R.string.user_error_invalid_user_detail));
                            }
                        }, error -> {
                    pb_UserLoader.setVisibility(View.GONE);
                    try {
                        if (error.networkResponse != null) {
                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject jsonObject = new JSONObject(responseBody);
                            showToast(jsonObject.getString("message"));
                        } else {
                            showToast(getString(R.string.user_error_invalid_user_detail));
                        }
                    } catch (Exception e) {
                        showToast(getString(R.string.user_error_invalid_user_detail));
                    }
                }) {
                    @Override
                    public byte[] getBody() {
                        return LoginDetail.toString().getBytes();
                    }

                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> params = new HashMap<>();
                        params.put("Content-Type", "application/json; charset=utf-8");
                        return params;
                    }
                };

                stringRequest.setShouldCache(false);
                ApplicationContext.getInstance().addToRequestQueue(stringRequest, "SignIn");
            }

        } catch (Exception e) {
            pb_UserLoader.setVisibility(View.GONE);
            showToast(getString(R.string.common_error));
        }

    }

    @Override
    public void ForgetPassword(JSONObject ForgetPasswordDetail) {
        try {
            if (ForgetPasswordDetail != null) {
                pb_UserLoader.setVisibility(View.VISIBLE);
                StringRequest stringRequest = new StringRequest(Request.Method.PUT, ConstantFields.mForgotPassword,
                        response -> {
                            pb_UserLoader.setVisibility(View.GONE);
                            if (fromSocialLogin) {
                                successLogin();
                            } else {
                                showToast(response);
                            }
                        }, error -> {
                    pb_UserLoader.setVisibility(View.GONE);
                    try {
                        if (error.networkResponse != null) {
                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject jsonObject = new JSONObject(responseBody);
                            showToast(jsonObject.getString("message"));
                            //showToast(getString(R.string.user_forget_password_error));
                        } else {
                            showToast(getString(R.string.user_error_invalid_user_detail));
                        }
                    } catch (Exception e) {
                        showToast(getString(R.string.user_error_invalid_user_detail));
                    }
                }) {
                    @Override
                    public byte[] getBody() {
                        return ForgetPasswordDetail.toString().getBytes();
                    }

                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> params = new HashMap<>();
                        params.put("Content-Type", "application/json; charset=utf-8");
                        return params;
                    }
                };

                stringRequest.setShouldCache(false);
                ApplicationContext.getInstance().addToRequestQueue(stringRequest, "ForgetPassword");
            }

        } catch (Exception e) {
            pb_UserLoader.setVisibility(View.GONE);
            showToast(getString(R.string.common_error));
        }
    }

    private void showToast(String message) {
        ConstantFields.CustomToast(message, ActivityLogin.this);
    }

    private void loadAccountInformation() {
        if (!ConstantDataSaver.mRetrieveSharedPreferenceString(ActivityLogin.this.getApplicationContext(), ConstantFields.mTokenTag).equals("Empty")) {
            StringRequest stringRequest = new StringRequest(Request.Method.GET, ConstantFields.getCustomerDetail(ConstantFields.currentLanguage().equals("en")),
                    response -> {

                        ModelAccountDetail modelAccountDetail = ConstantDataParser.getAccountDetail(response);

                        if (modelAccountDetail != null) {
                            if (modelAccountDetail.getEmailId() != null) {
                                ConstantDataSaver.mStoreSharedPreferenceString(ActivityLogin.this.getApplicationContext(),
                                        ConstantFields.mCustomerEmailIdTag, modelAccountDetail.getEmailId());

                                ConstantDataSaver.mStoreSharedPreferenceString(ActivityLogin.this.getApplicationContext(),
                                        ConstantFields.mCustomerAccountTag, response);

                            }
                        }
                        finish();
                    }, error -> {
                if (error.networkResponse != null) {
                    if (error.networkResponse.statusCode == 401) {
                        ConstantDataSaver.mRemoveSharedPreferenceString(ActivityLogin.this.getApplicationContext(), ConstantFields.mTokenTag);
                    }
                }
                finish();
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<>();
                    params.put("Content-Type", "application/json");
                    params.put("Authorization", "Bearer "
                            + ConstantDataSaver.mRetrieveSharedPreferenceString(ActivityLogin.this.getApplicationContext(),
                            ConstantFields.mTokenTag));
                    return params;
                }
            };

            stringRequest.setShouldCache(false);
            ApplicationContext.getInstance().addToRequestQueue(stringRequest, "CustomerAccountInformation");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        socialLoginSessionLogout();
    }

    private void socialLoginSessionLogout() {
        if (mType != null)
            if (mType.equals("Facebook")) {
                facebook_logout();
            } else {
                twitter_logout();
            }
    }

    private void facebook_logout() {
         /*Facebook logout*/
        LoginManager.getInstance().logOut();
    }

    private void twitter_logout() {
        /*Twitter Logout*/
        TwitterCore.getInstance().getSessionManager().clearActiveSession();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(AppLanguageSupport.onAttach(base));
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(
                    "ar".equals(AppLanguageSupport.getLanguage(ActivityLogin.this)) ?
                            View.LAYOUT_DIRECTION_RTL : View.LAYOUT_DIRECTION_LTR);
        }
    }
}
