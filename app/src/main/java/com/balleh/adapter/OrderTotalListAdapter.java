package com.balleh.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.balleh.R;
import com.balleh.additional.Constant;

import java.util.ArrayList;

public class OrderTotalListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Activity activity;
    private ArrayList<String[]> totalList;

    public OrderTotalListAdapter(Activity activity, ArrayList<String[]> totalList) {
        this.activity = activity;
        this.totalList = totalList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new TotalListViewHolder(LayoutInflater.from(activity).inflate(R.layout.rc_row_order_total, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        TotalListViewHolder totalListViewHolder = (TotalListViewHolder) holder;

        if (totalList.get(position)[0] != null)
            totalListViewHolder.atv_TotalTitle.setText(totalList.get(position)[0]);

        if (totalList.get(position)[1] != null) {
            String price = "SAR " + totalList.get(position)[1];
            totalListViewHolder.atv_TotalValue.setText(price);
        }
    }

    @Override
    public int getItemCount() {
        return totalList.size();
    }

    private class TotalListViewHolder extends RecyclerView.ViewHolder {

        private AppCompatTextView atv_TotalTitle, atv_TotalValue;

        TotalListViewHolder(View itemView) {
            super(itemView);
            atv_TotalTitle = itemView.findViewById(R.id.atv_total_title_order);
            atv_TotalValue = itemView.findViewById(R.id.atv_total_value_order);
            fontSetup(atv_TotalTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);
            fontSetup(atv_TotalValue, Constant.LIGHT, Constant.C_TEXT_VIEW);
        }
    }

    private void fontSetup(Object o_SetFont, int type, int which) {
        Constant.SetFontStyle(o_SetFont, type, which);
    }
}
