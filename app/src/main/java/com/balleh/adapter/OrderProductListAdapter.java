package com.balleh.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.balleh.R;
import com.balleh.additional.Constant;
import com.balleh.model.ModelOrderDetail;

import java.util.ArrayList;

public class OrderProductListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Activity activity;
    private ArrayList<ModelOrderDetail> orderProductList;

    public OrderProductListAdapter(Activity activity, ArrayList<ModelOrderDetail> orderProductList) {
        this.activity = activity;
        this.orderProductList = orderProductList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new OrderProductList(LayoutInflater.from(activity).inflate(R.layout.rc_row_order_product, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        OrderProductList orderProductListViewHolder = (OrderProductList) holder;
        orderProductListViewHolder.atv_ProductOrderTitle.setText(orderProductList.get(position).getName());

        String price = "SAR " + orderProductList.get(position).getPrice();
        String quantity = "Quantity : " + orderProductList.get(position).getQuantity();

        orderProductListViewHolder.atv_ProductOrderQuantity.setText(quantity);
        orderProductListViewHolder.atv_ProductOrderPrice.setText(price);
    }

    @Override
    public int getItemCount() {
        return orderProductList.size();
    }


    private class OrderProductList extends RecyclerView.ViewHolder {

        ImageView iv_ProductOrderList;
        AppCompatTextView atv_ProductOrderTitle, atv_ProductOrderPrice, atv_ProductOrderQuantity;

        OrderProductList(View itemView) {
            super(itemView);
            iv_ProductOrderList = itemView.findViewById(R.id.iv_product_image_order);
            atv_ProductOrderTitle = itemView.findViewById(R.id.atv_product_title_order);
            atv_ProductOrderPrice = itemView.findViewById(R.id.atv_product_price_order);
            atv_ProductOrderQuantity = itemView.findViewById(R.id.atv_product_qty_order);

            fontSetup(atv_ProductOrderTitle,Constant.LIGHT,Constant.C_TEXT_VIEW);
            fontSetup(atv_ProductOrderPrice,Constant.LIGHT,Constant.C_TEXT_VIEW);
            fontSetup(atv_ProductOrderQuantity,Constant.LIGHT,Constant.C_TEXT_VIEW);
        }
    }

    private void fontSetup(Object o_SetFont, int type, int which) {
        Constant.SetFontStyle(o_SetFont, type, which);
    }
}
