package com.balleh.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.balleh.R;
import com.balleh.additional.Constant;
import com.balleh.additional.instentTransfer.ProductListHelper;
import com.balleh.model.ModelHomeMenuCategory;

import java.util.ArrayList;

public class ProductListSubCategoryList extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<ModelHomeMenuCategory> subCategoryList;
    private Activity activity;
    private ProductListHelper productListHelper;

    public ProductListSubCategoryList(Activity activity, ArrayList<ModelHomeMenuCategory> subCategoryList, ProductListHelper productListHelper) {
        this.activity = activity;
        this.subCategoryList = subCategoryList;
        this.productListHelper = productListHelper;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SubCategoryListViewHolder(LayoutInflater.from(activity).inflate(R.layout.rc_row_list_sub_category_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        SubCategoryListViewHolder subCategoryListViewHolder = (SubCategoryListViewHolder) holder;

        subCategoryListViewHolder.atv_SubCategoryTitle.setText(subCategoryList.get(position).getName());

        subCategoryListViewHolder.atv_SubCategoryTitle.setOnClickListener(view -> productListHelper.toProductList(Integer.valueOf(subCategoryList.get(position).getId()), subCategoryList.get(position).getProductCount()));
    }

    @Override
    public int getItemCount() {
        return subCategoryList.size();
    }

    class SubCategoryListViewHolder extends RecyclerView.ViewHolder {

        AppCompatTextView atv_SubCategoryTitle;

        SubCategoryListViewHolder(View itemView) {
            super(itemView);
            atv_SubCategoryTitle = itemView.findViewById(R.id.atv_sub_category_list_title);
            fontSetup(atv_SubCategoryTitle,Constant.LIGHT,Constant.C_TEXT_VIEW);
        }
    }

    private void fontSetup(Object o_SetFont, int type, int which) {
        Constant.SetFontStyle(o_SetFont, type, which);
    }
}
