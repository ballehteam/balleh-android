package com.balleh.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.balleh.R;
import com.balleh.additional.Constant;
import com.balleh.model.ModelAddress;

import java.util.ArrayList;

public class AddressListSpinnerAdapter extends ArrayAdapter<ModelAddress> {
    private ArrayList<ModelAddress> data;
    private Activity context;

    public AddressListSpinnerAdapter(Activity context, int resource, ArrayList<ModelAddress> data) {
        super(context, resource, data);
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View row = convertView;
        if (row == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            row = inflater.inflate(R.layout.spinner_address, parent, false);
        }
        if (data.get(position) != null) {
            AppCompatTextView CountryName = row.findViewById(R.id.atv_address_detail);
            if (CountryName != null) {
                CountryName.setText(data.get(position).getAddress());
                CountryName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand_more_black_24dp, 0);
                fontSetup(CountryName, Constant.LIGHT, Constant.C_TEXT_VIEW);
            }
        }
        return row;
    }

    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
        View row = convertView;
        if (row == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            row = inflater.inflate(R.layout.spinner_address, parent, false);
        }

        if (data.get(position) != null) {
            AppCompatTextView CountryName = row.findViewById(R.id.atv_address_detail);
            int color = ContextCompat.getColor(context, android.R.color.white);
            CountryName.setBackgroundColor(color);
            CountryName.setText(data.get(position).getAddress());
            fontSetup(CountryName, Constant.LIGHT, Constant.C_TEXT_VIEW);
        }
        return row;
    }

    private void fontSetup(Object o_SetFont, int type, int which) {
        Constant.SetFontStyle(o_SetFont, type, which);
    }
}
