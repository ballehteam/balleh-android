package com.balleh.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.balleh.R;
import com.balleh.additional.Constant;
import com.balleh.additional.ConstantFields;
import com.balleh.additional.instentTransfer.MenuHandler;
import com.balleh.model.ModelHomeMenuCategoryList;

import java.util.ArrayList;

public class HomeLSCustomExpandableViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Activity activity;
    private ArrayList<ModelHomeMenuCategoryList> mParentList;
    private MenuHandler menuHandler;

    HomeLSCustomExpandableViewAdapter(Activity activity, ArrayList<ModelHomeMenuCategoryList> list, MenuHandler menuHandler) {
        this.activity = activity;
        this.mParentList = list;
        this.menuHandler = menuHandler;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CustomExpandableRowHolder(LayoutInflater.from(activity).inflate(R.layout.rc_row_home_custom_expandable_view, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        CustomExpandableRowHolder customExpandableRowHolder = (CustomExpandableRowHolder) holder;

        if (mParentList.get(position).getHomeMenuCategory().isOpened()) {
            if (ConstantFields.currentLanguage().equals("ar")) {
                customExpandableRowHolder.atv_CategoryTitle.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(activity, R.drawable.ic_expand_less_black_24dp), null, null, null);
                customExpandableRowHolder.subListHolder.setVisibility(View.VISIBLE);
            } else {
                customExpandableRowHolder.atv_CategoryTitle.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(activity, R.drawable.ic_expand_more_black_24dp), null);
                customExpandableRowHolder.subListHolder.setVisibility(View.VISIBLE);
            }
        } else {
            if (ConstantFields.currentLanguage().equals("ar")) {
                customExpandableRowHolder.atv_CategoryTitle.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(activity, R.drawable.ic_expand_less_black_24dp), null, null, null);
                customExpandableRowHolder.subListHolder.setVisibility(View.GONE);
            } else {
                customExpandableRowHolder.atv_CategoryTitle.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(activity, R.drawable.ic_expand_more_black_24dp), null);
                customExpandableRowHolder.subListHolder.setVisibility(View.GONE);
            }

        }

        customExpandableRowHolder.atv_CategoryTitle.setText(mParentList.get(position).getHomeMenuCategory().getName());
        customExpandableRowHolder.subListHolder.setLayoutManager(new LinearLayoutManager(activity));
        customExpandableRowHolder.subListHolder.setAdapter(new HomeLSSubCategoryAdapter(activity, mParentList.get(position).getHomeMenuCategories(), menuHandler));

    }

    @Override
    public int getItemCount() {
        return mParentList.size();
    }

    private class CustomExpandableRowHolder extends RecyclerView.ViewHolder {
        AppCompatTextView atv_CategoryTitle;
        RecyclerView subListHolder;

        CustomExpandableRowHolder(View itemView) {
            super(itemView);
            atv_CategoryTitle = itemView.findViewById(R.id.atv_custom_expandable_row_title);
            subListHolder = itemView.findViewById(R.id.rc_custom_expandable_row_child_list);
            fontSetup(atv_CategoryTitle,Constant.LIGHT,Constant.C_TEXT_VIEW);
            atv_CategoryTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getAdapterPosition() != -1) {
                        if (!mParentList.get(getAdapterPosition()).getHomeMenuCategory().isOpened()) {
                            mParentList.get(getAdapterPosition()).getHomeMenuCategory().setOpened(true);
                        } else {
                            mParentList.get(getAdapterPosition()).getHomeMenuCategory().setOpened(false);
                        }
                        notifyItemChanged(getAdapterPosition());
                    }
                }
            });
        }
    }

    private void fontSetup(Object o_SetFont, int type, int which) {
        Constant.SetFontStyle(o_SetFont, type, which);
    }

}
