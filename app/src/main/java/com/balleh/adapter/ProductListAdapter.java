package com.balleh.adapter;

import android.app.Activity;
import android.arch.persistence.room.Room;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.balleh.R;
import com.balleh.additional.Constant;
import com.balleh.additional.ConstantDataSaver;
import com.balleh.additional.ConstantFields;
import com.balleh.additional.ImageLoader;
import com.balleh.additional.db.wishlist.WishListDBHandler;
import com.balleh.additional.db.wishlist.WishListDao;
import com.balleh.additional.db.wishlist.WishListRoom;
import com.balleh.additional.instentTransfer.MenuHandler;
import com.balleh.model.ModelProductDetailList;

import java.util.ArrayList;

public class ProductListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private int GRID = 1;
    private int LIST = 2;
    private Activity activity;
    private boolean mType;
    private ArrayList<ModelProductDetailList> mProductList;
    private OnLoadMoreListener onLoadMoreListener;
    private int visibleThreshold = 10;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private MenuHandler menuHandler;
    private WishListDao wishListDao;

    public ProductListAdapter(Activity activity, ArrayList<ModelProductDetailList> list, boolean type, MenuHandler menuHandler, RecyclerView recyclerView) {
        this.activity = activity;
        this.mProductList = list;
        this.mType = type;
        this.menuHandler = menuHandler;

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {

                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }
        WishListDBHandler wishListDBHandler = Room.databaseBuilder(activity, WishListDBHandler.class, "WishList")
                .allowMainThreadQueries().build();
        wishListDao = wishListDBHandler.getWishList();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder viewHolder;

        if (viewType == GRID) {
            viewHolder = new Grid_Holder(LayoutInflater.from(activity).inflate(R.layout.rc_row_grid_product_list, parent, false));
        } else if (viewType == LIST) {
            viewHolder = new List_Holder(LayoutInflater.from(activity).inflate(R.layout.rc_row_list_product_list, parent, false));
        } else {
            viewHolder = new EmptyProductList(LayoutInflater.from(activity).inflate(R.layout.rc_row_empty, parent, false));
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (holder.getItemViewType() == GRID) {
            Grid_Holder gridHolder = (Grid_Holder) holder;

            if (!ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mTokenTag).equals("Empty")) {
                if (wishListDao.isAvailable(mProductList.get(position).getSku())) {
                    gridHolder.ib_ProductFavGrid.setImageResource(R.drawable.ic_favorite_selected_24dp);
                } else {
                    gridHolder.ib_ProductFavGrid.setImageResource(R.drawable.ic_favorite_grey_500_24dp);
                }
            }

            if (mProductList.get(position).getAttribute_result() != null) {
                if (mProductList.get(position).getAttribute_result().size() > 0) {
                    for (int i = 0; i < mProductList.get(position).getAttribute_result().size(); i++) {
                        if (mProductList.get(position).getAttribute_result().get(i)[0].trim().toLowerCase().equals("small_image")) {
                            ImageLoader.glide_image_loader(ConstantFields.ImageURL + mProductList.get(position).getAttribute_result().get(i)[1],
                                    gridHolder.iv_ProductImageGrid, "Original");
                        }
                    }
                }
            }

            if (mProductList.get(position).getAttribute_result() != null) {
                if (mProductList.get(position).getAttribute_result().size() > 0) {

                    for (int i = 0; i < mProductList.get(position).getAttribute_result().size(); i++) {

                        if (mProductList.get(position).getAttribute_result().get(i)[0].trim().toLowerCase().equals("special_price")) {
                            mProductList.get(position).setSpecial_price(Double.valueOf(mProductList.get(position).getAttribute_result().get(i)[1].trim()).intValue());
                        }

                        if (ConstantFields.currentLanguage().equals("ar")) {
                            if (!mProductList.get(position).getAr_title().equals("a")) {
                                gridHolder.atv_ProductTitleGird.setText(mProductList.get(position).getAr_title());
                            } else {
                                if (mProductList.get(position).getAttribute_result().get(i)[0] != null) {
                                    if (mProductList.get(position).getAttribute_result().get(i)[0].trim().toLowerCase().equals("meta_title")) {
                                        String title = mProductList.get(position).getAttribute_result().get(i)[1];
                                        if (title.substring(0, 3).toLowerCase().equals("buy")) {
                                            title = title.substring(4);
                                        }
                                        gridHolder.atv_ProductTitleGird.setText(title);
                                    }
                                }
                            }
                        } else {
                            if (!mProductList.get(position).getEn_title().equals("a")) {
                                gridHolder.atv_ProductTitleGird.setText(mProductList.get(position).getEn_title());
                            } else {
                                if (mProductList.get(position).getAttribute_result().get(i)[0] != null) {
                                    if (mProductList.get(position).getAttribute_result().get(i)[0].trim().toLowerCase().equals("meta_title")) {
                                        String title = mProductList.get(position).getAttribute_result().get(i)[1];
                                        if (title.substring(0, 3).toLowerCase().equals("buy")) {
                                            title = title.substring(4);
                                        }
                                        gridHolder.atv_ProductTitleGird.setText(title);
                                    }
                                }
                            }
                        }
                    }

                    if (gridHolder.atv_ProductTitleGird.getText().toString().length() == 0) {
                        gridHolder.atv_ProductTitleGird.setText(mProductList.get(position).getName());
                    }

                } else {
                    gridHolder.atv_ProductTitleGird.setText(mProductList.get(position).getName());
                }
            } else {
                gridHolder.atv_ProductTitleGird.setText(mProductList.get(position).getName());
            }

            if (mProductList.get(position).getSpecial_price() != 0) {
                gridHolder.atv_ProductPriceGrid.setVisibility(View.VISIBLE);
                gridHolder.atv_ProductPriceGrid.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                gridHolder.atv_ProductPriceGrid.setText(String.valueOf("SAR " + mProductList.get(position).getPrice()));
                gridHolder.atv_ProductSpecialPriceGrid.setText(String.valueOf("SAR " + mProductList.get(position).getSpecial_price()));
            } else {
                gridHolder.atv_ProductSpecialPriceGrid.setText(String.valueOf("SAR " + mProductList.get(position).getPrice()));
                gridHolder.atv_ProductPriceGrid.setVisibility(View.GONE);
            }

        } else if (holder.getItemViewType() == LIST) {
            List_Holder listHolder = (List_Holder) holder;

            if (!ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mTokenTag).equals("Empty")) {
//                if (WishList.getInstance(activity.getApplicationContext()).getSizeWishList() > 0)
//                    if (WishList.getInstance(activity.getApplicationContext()).checking_wish_list(mProductList.get(position).getSku())) {
//                        listHolder.ib_ProductFavList.setImageResource(R.drawable.ic_favorite_selected_24dp);
//                    } else {
//                        listHolder.ib_ProductFavList.setImageResource(R.drawable.ic_favorite_grey_500_24dp);
//                    }
                if (wishListDao.isAvailable(mProductList.get(position).getSku())) {
                    listHolder.ib_ProductFavList.setImageResource(R.drawable.ic_favorite_selected_24dp);
                } else {
                    listHolder.ib_ProductFavList.setImageResource(R.drawable.ic_favorite_grey_500_24dp);
                }
            }

            if (mProductList.get(position).getAttribute_result() != null) {
                if (mProductList.get(position).getAttribute_result().size() > 0) {
                    for (int i = 0; i < mProductList.get(position).getAttribute_result().size(); i++) {
                        if (mProductList.get(position).getAttribute_result().get(i)[0].trim().toLowerCase().equals("small_image")) {
                            ImageLoader.glide_image_loader(ConstantFields.ImageURL + mProductList.get(position).getAttribute_result().get(i)[1],
                                    listHolder.iv_ProductImageList, "Original");
                        }
                    }
                }
            }

            if (mProductList.get(position).getAttribute_result() != null) {
                if (mProductList.get(position).getAttribute_result().size() > 0) {
                    for (int i = 0; i < mProductList.get(position).getAttribute_result().size(); i++) {
                        if (mProductList.get(position).getAttribute_result().get(i)[0].trim().toLowerCase().equals("special_price")) {
                            mProductList.get(position).setSpecial_price(Double.valueOf(mProductList.get(position).getAttribute_result().get(i)[1].trim()).intValue());
                        }

                        if (ConstantFields.currentLanguage().equals("ar")) {
                            if (!mProductList.get(position).getAr_title().equals("a")) {
                                listHolder.atv_ProductTitleList.setText(mProductList.get(position).getAr_title());
                            } else {
                                if (mProductList.get(position).getAttribute_result().get(i)[0] != null) {
                                    if (mProductList.get(position).getAttribute_result().get(i)[0].trim().toLowerCase().equals("meta_title")) {
                                        String title = mProductList.get(position).getAttribute_result().get(i)[1];
                                        if (title.substring(0, 3).toLowerCase().equals("buy")) {
                                            title = title.substring(4);
                                        }
                                        listHolder.atv_ProductTitleList.setText(title);
                                    }
                                }
                            }
                        } else {
                            if (!mProductList.get(position).getEn_title().equals("a")) {
                                listHolder.atv_ProductTitleList.setText(mProductList.get(position).getEn_title());
                            } else {
                                if (mProductList.get(position).getAttribute_result().get(i)[0] != null) {
                                    if (mProductList.get(position).getAttribute_result().get(i)[0].trim().toLowerCase().equals("meta_title")) {
                                        String title = mProductList.get(position).getAttribute_result().get(i)[1];
                                        if (title.substring(0, 3).toLowerCase().equals("buy")) {
                                            title = title.substring(4);
                                        }
                                        listHolder.atv_ProductTitleList.setText(title);
                                    }
                                }
                            }
                        }

                        /*if (mProductList.get(position).getAttribute_result().get(i)[0] != null) {
                            if (mProductList.get(position).getAttribute_result().get(i)[0].toLowerCase().equals("meta_title")) {
                                String title = mProductList.get(position).getAttribute_result().get(i)[1];
                                if (title.substring(0, 3).toLowerCase().equals("buy")) {
                                    title = title.substring(4);
                                }
                                listHolder.atv_ProductTitleList.setText(title);

                            } else {
                                listHolder.atv_ProductTitleList.setText(mProductList.get(position).getName());
                            }
                        } else {
                            listHolder.atv_ProductTitleList.setText(mProductList.get(position).getName());
                        }*/
                    }

                    if (listHolder.atv_ProductTitleList.getText().toString().length() == 0) {
                        listHolder.atv_ProductTitleList.setText(mProductList.get(position).getName());
                    }

                } else {
                    listHolder.atv_ProductTitleList.setText(mProductList.get(position).getName());
                }
            } else {
                listHolder.atv_ProductTitleList.setText(mProductList.get(position).getName());
            }

            if (mProductList.get(position).getSpecial_price() != 0) {
                listHolder.atv_ProductPriceList.setVisibility(View.VISIBLE);
                listHolder.atv_ProductPriceList.setText(String.valueOf("SAR " + mProductList.get(position).getPrice()));
                listHolder.atv_ProductPriceList.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                listHolder.atv_ProductSpecialPriceList.setText(String.valueOf("SAR " + mProductList.get(position).getSpecial_price()));
            } else {
                listHolder.atv_ProductSpecialPriceList.setText(String.valueOf("SAR " + mProductList.get(position).getPrice()));
                listHolder.atv_ProductPriceList.setVisibility(View.GONE);
            }
        } else {
            EmptyProductList empty_view = (EmptyProductList) holder;
            empty_view.atv_EmptyMessage.setText(activity.getString(R.string.product_list_error));
        }
    }

    @Override
    public int getItemCount() {
        if (mProductList != null) {
            if (mProductList.size() > 0) {
                return mProductList.size();
            } else {
                return 1;
            }
        } else {
            return 1;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mProductList != null && !mProductList.isEmpty()) {
            if (mProductList.get(position) != null) {
                if (mType) {
                    return GRID;
                } else {
                    return LIST;
                }
            } else {
                return 0;
            }
        } else {
            return 3;
        }
    }

    private class Grid_Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView iv_ProductImageGrid;
        AppCompatTextView atv_ProductTitleGird, atv_ProductPriceGrid, atv_ProductSpecialPriceGrid;
        ImageButton ib_ProductFavGrid;

        Grid_Holder(View view) {
            super(view);
            iv_ProductImageGrid = view.findViewById(R.id.iv_product_image_product_list_gird_type);
            atv_ProductTitleGird = view.findViewById(R.id.atv_product_title_product_list_gird_type);
            atv_ProductPriceGrid = view.findViewById(R.id.atv_product_price_product_list_gird_type);
            atv_ProductSpecialPriceGrid = view.findViewById(R.id.atv_product_special_price_product_list_gird_type);
            ib_ProductFavGrid = view.findViewById(R.id.ib_product_wish_list_product_list_gird_type);
            ib_ProductFavGrid.setOnClickListener(this);
            iv_ProductImageGrid.setOnClickListener(this);
            atv_ProductTitleGird.setOnClickListener(this);
            atv_ProductPriceGrid.setOnClickListener(this);
            atv_ProductSpecialPriceGrid.setOnClickListener(this);

            fontSetup(atv_ProductTitleGird,Constant.LIGHT,1);
            fontSetup(atv_ProductPriceGrid,Constant.LIGHT,1);
            fontSetup(atv_ProductSpecialPriceGrid,Constant.LIGHT,1);
        }

        @Override
        public void onClick(View v) {
            int id = v.getId();
            switch (id) {
                case R.id.ib_product_wish_list_product_list_gird_type:
                    if (getAdapterPosition() != -1)
                        addToWishList(ib_ProductFavGrid, getAdapterPosition());
                    break;
                case R.id.iv_product_image_product_list_gird_type:
                    if (getAdapterPosition() != -1)
                        toProductDetail(getAdapterPosition());
                    break;
                case R.id.atv_product_title_product_list_gird_type:
                    if (getAdapterPosition() != -1)
                        toProductDetail(getAdapterPosition());
                    break;
                case R.id.atv_product_price_product_list_gird_type:
                    if (getAdapterPosition() != -1)
                        toProductDetail(getAdapterPosition());
                    break;
                case R.id.atv_product_special_price_product_list_gird_type:
                    if (getAdapterPosition() != -1)
                        toProductDetail(getAdapterPosition());
                    break;
            }
        }
    }

    private class List_Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView iv_ProductImageList;
        AppCompatTextView atv_ProductTitleList, atv_ProductPriceList, atv_ProductSpecialPriceList;
        ImageButton ib_ProductFavList;

        List_Holder(View view) {
            super(view);
            iv_ProductImageList = view.findViewById(R.id.iv_product_image_list_type);
            atv_ProductTitleList = view.findViewById(R.id.atv_product_title_list_type);
            atv_ProductPriceList = view.findViewById(R.id.atv_product_price_list_type);
            atv_ProductSpecialPriceList = view.findViewById(R.id.atv_product_special_price_list_type);
            ib_ProductFavList = view.findViewById(R.id.ib_product_wish_list_list_type);
            ib_ProductFavList.setOnClickListener(this);
            iv_ProductImageList.setOnClickListener(this);
            atv_ProductTitleList.setOnClickListener(this);
            atv_ProductPriceList.setOnClickListener(this);
            atv_ProductSpecialPriceList.setOnClickListener(this);

            fontSetup(atv_ProductTitleList,Constant.LIGHT,1);
            fontSetup(atv_ProductPriceList,Constant.LIGHT,1);
            fontSetup(atv_ProductSpecialPriceList,Constant.LIGHT,1);
        }

        @Override
        public void onClick(View v) {
            int id = v.getId();
            switch (id) {
                case R.id.ib_product_wish_list_list_type:
                    if (getAdapterPosition() != -1)
                        addToWishList(ib_ProductFavList, getAdapterPosition());
                    break;
                case R.id.iv_product_image_list_type:
                    if (getAdapterPosition() != -1)
                        toProductDetail(getAdapterPosition());
                    break;
                case R.id.atv_product_title_list_type:
                    if (getAdapterPosition() != -1)
                        toProductDetail(getAdapterPosition());
                    break;
                case R.id.atv_product_price_list_type:
                    if (getAdapterPosition() != -1)
                        toProductDetail(getAdapterPosition());
                    break;
                case R.id.atv_product_special_price_list_type:
                    if (getAdapterPosition() != -1)
                        toProductDetail(getAdapterPosition());
                    break;
            }
        }
    }

    private class EmptyProductList extends RecyclerView.ViewHolder {
        AppCompatTextView atv_EmptyMessage;

        EmptyProductList(View view) {
            super(view);
            atv_EmptyMessage = view.findViewById(R.id.atv_empty_message);
            fontSetup(atv_EmptyMessage,Constant.LIGHT,1);
        }
    }

    private void toProductDetail(int position) {
        menuHandler.toProductDetail(mProductList.get(position).getSku());
    }

    private void addToWishList(ImageButton ib_ProductFav, int position) {
        if (!ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mTokenTag).equals("Empty")) {
//            if (!WishList.getInstance(activity.getApplicationContext()).checking_wish_list(mProductList.get(position).getSku())) {
//                WishList.getInstance(activity.getApplicationContext()).add_to_wish_list(mProductList.get(position).getSku(),
            if (!wishListDao.isAvailable(mProductList.get(position).getSku())) {

                WishListRoom wishListRoom = new WishListRoom();
                wishListRoom.setProductId(mProductList.get(position).getSku());
                wishListRoom.setProductDetails(mProductList.get(position).getProductDetail());
                wishListDao.insert(wishListRoom);
                String message = mProductList.get(position).getName() + " " + activity.getString(R.string.wish_list_add);
                showToast(message);
                ib_ProductFav.setImageResource(R.drawable.ic_favorite_selected_24dp);
            } else {
              String message = mProductList.get(position).getName() + " " + activity.getString(R.string.wish_list_remove);
                showToast(message);
                //WishList.getInstance(activity.getApplicationContext()).remove_from_wish_list(mProductList.get(position).getSku().replace("-",""));
                wishListDao.deleteProduct(mProductList.get(position).getSku());
                ib_ProductFav.setImageResource(R.drawable.ic_favorite_grey_500_24dp);
            }
        } else {
            menuHandler.toSignIn();
        }
    }

    private void showToast(String message) {
        ConstantFields.CustomToast(message, activity);
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public interface OnLoadMoreListener {
        void onLoadMore();
    }

    public void setLoaded() {
        loading = false;
    }

    private void fontSetup(Object o_SetFont, int type, int which) {
        Constant.SetFontStyle(o_SetFont, type, which);
    }
}
