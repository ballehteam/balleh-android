package com.balleh.additional.api;

import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by user on 7/2/2018.
 * Retrofit Interface - API caller
 */

public interface ApiInterface {

    /*Post*/

    /*Payment gateway*/
    @Multipart
    @POST("index.php/rest/en/v1/charge")
    Call<String> PaymentCharge(@Part("data") String value);

    /*Post*/

}


