package com.balleh.additional;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import com.balleh.additional.instentTransfer.CustomerResponse;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class CustomerRestHandler {

    private CustomerResponse customerResponse;
    private String source, post_data;
    private Activity activity;

    public void load(String url, CustomerResponse customerResponse, String data, String type, String source, Activity activity) {
        this.customerResponse = customerResponse;
        if (data != null) {
            this.post_data = data;
        }
        this.activity = activity;

        this.source = source;
        if (type.equals("get")) {
            new AsyncTaskForGet().execute(url);
        } else {
            new AsyncTaskForPost().execute(url);
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class AsyncTaskForGet extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... urls) {
            String mData;
            try {

                URL mUrl = new URL(urls[0]);
                Log.d("GET ", urls[0]);
                HttpURLConnection mUrlConnection = (HttpURLConnection) mUrl.openConnection();
                mUrlConnection.setRequestMethod("GET");
                mUrlConnection.setRequestProperty("Content-Type", "application/json");
                mUrlConnection.setRequestProperty("Authorization", "Bearer " + ConstantDataSaver
                        .mRetrieveSharedPreferenceString(activity.getApplicationContext(),
                                ConstantFields.mTokenTag));

                mUrlConnection.setReadTimeout(15000);

                if (mUrlConnection.getInputStream() != null) {
                    BufferedReader mBufferedReader = new BufferedReader(new InputStreamReader(mUrlConnection.getInputStream()));
                    mData = inputStreamToStringConversion(mBufferedReader);
                } else {
                    mData = "Error";
                }
                Log.d("GET Response", mData);
                return mData;
            } catch (Exception e) {
                Log.e("GET Exception ", e.toString());
                return null;
            }
        }

        @Override
        protected void onPostExecute(String data) {
            customerResponse.getResult(data, source);
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class AsyncTaskForPost extends AsyncTask<String, Void, String> {


        @Override
        protected String doInBackground(String... params) {
            try {
                URL url = new URL(params[0]);
                Log.d("POST url", url.toString());
                HttpURLConnection mUrlConnection = (HttpURLConnection) url.openConnection();
                mUrlConnection.setRequestMethod("POST");
                mUrlConnection.setRequestProperty("Content-Type", "application/json");
                mUrlConnection.setRequestProperty("Authorization", "Bearer " + ConstantDataSaver
                        .mRetrieveSharedPreferenceString(activity.getApplicationContext(),
                                ConstantFields.mTokenTag));

                mUrlConnection.setDoInput(true);
                mUrlConnection.setDoOutput(true);
                mUrlConnection.setReadTimeout(15000);
                OutputStream mOutputStream = mUrlConnection.getOutputStream();

                BufferedWriter mBufferedWriter = new BufferedWriter(new OutputStreamWriter(mOutputStream, "UTF-8"));
                mBufferedWriter.write(post_data);
                mBufferedWriter.close();
                mOutputStream.close();

                String mResponse;
                if (mUrlConnection.getResponseCode() == 200) {
                    mResponse = inputStreamToStringConversion(new BufferedReader(new InputStreamReader(mUrlConnection.getInputStream())));
                } else {
                    Log.e("doInBackground: ", inputStreamToStringConversion(new BufferedReader(new InputStreamReader(mUrlConnection.getInputStream()))) + "");
                    mResponse = "Error";
                }

                Log.e("doInBackground: ", mResponse);
                return mResponse;
            } catch (Exception e) {
                Log.e("doInBackground: ", e.toString());
                return null;
            }
        }

        @Override
        protected void onPostExecute(String s) {
            customerResponse.getResult(s, source);
        }
    }

    private static String inputStreamToStringConversion(BufferedReader bufferedReader) {
        try {
            String line;
            StringBuilder result = new StringBuilder();
            while ((line = bufferedReader.readLine()) != null) {
                result.append(line);
            }
            return result.toString();
        } catch (Exception e) {
            return null;
        }
    }
}
