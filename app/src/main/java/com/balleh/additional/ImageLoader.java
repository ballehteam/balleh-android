package com.balleh.additional;

import android.widget.ImageView;

import com.balleh.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

/**
 * Created by Sasikumar on 3/20/2018.
 * Glide Image Loader handler
 */

public class ImageLoader {

    public static void glide_image_loader(String url, final ImageView imageView, String type) {

        /*Type - 3rd argument
        *
        * Original - Without Resize
        *
        * Static - Fixed size like thumbnail image
        *
        * */

        Glide.with(ApplicationContext.getApplicationInstance())
                .load(url)
                .apply(getOption(type))
                .into(imageView);
    }

    private static RequestOptions getOption(String which) {
        RequestOptions options;
        switch (which) {
            case "Static":
                options = new RequestOptions()
                        .error(R.drawable.placeholder)
                        .override(300, 300)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .priority(Priority.IMMEDIATE);
                break;
            default:
                options = new RequestOptions()
                        .error(R.drawable.placeholder)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .priority(Priority.IMMEDIATE);
                break;
        }
        return options;

    }

}
