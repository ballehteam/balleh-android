package com.balleh.additional.instentTransfer;

public interface CheckOutHandler {

    void loadCartList();

    void askPurchaseType();

    void loadLogin();

    void loadAddressList();

    void loadShippingMethodList();

    void loadPaymentMethodList();

    void loadPlaceOrder(String response);

    void placeOrder(String OrderId);

    void loadPayment(String amount,String orderId,String paymentType);

    void backPressed(String type, String where);

    boolean networkChecker();

    void orderSuccess(String orderId);

}
