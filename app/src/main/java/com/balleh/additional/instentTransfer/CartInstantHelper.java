package com.balleh.additional.instentTransfer;

import com.balleh.model.ModelCartListProduct;

public interface CartInstantHelper {
    void cartUpdate(ModelCartListProduct CartDetail, String type);
}
