package com.balleh.additional.instentTransfer;

import java.util.ArrayList;

public interface MenuHandler {

    void changeLang(String toWhich);

    void toHome();

    void toSignIn();

    void toSignUp();

    void toDeals();

    void toProductList(int categoryId, String type, String filter);

    void loadFilter(int categoryId);

    void toLiveChat();

    void toTrackOrder();

    void toShare();

    void toProductDetail(String sku);

    void toProductImageList(ArrayList<String> imageList);

    void updateCart();

    void loadEditAccount();

    void loadChangePassword();

    void loadAddressBook();

    void reloadAddressBook();

    void loadAddress(String accountDetail, int id, String type);

    void loadOrderList();

    void loadSingleOrderHistory(String orderDetail);

    void loadDownloadableProduct();

    void loadAboutUs();

    void loadContactUs();

    void loadLogout();

    void backBtnPressed();

    boolean networkError();

    void visibility();

    void changeBottomView(int type);

}
