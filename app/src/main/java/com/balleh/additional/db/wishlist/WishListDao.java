package com.balleh.additional.db.wishlist;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created by user on 8/8/2018.
 * WishList DAO
 */

@Dao
public interface WishListDao {

    @Query("SELECT * From WishListDB")
    List<WishListRoom> getWishList();

    @Insert
    void insert(WishListRoom wishListRoom);

    @Query("DELETE FROM WISHLISTDB")
    void deleteAllRecord();

    @Query("DELETE FROM WISHLISTDB WHERE product_id=:product_id")
    void deleteProduct(String product_id);

    @Query("SELECT * FROM WishListDB Where product_id=:product_id")
    boolean isAvailable(String product_id);
}
