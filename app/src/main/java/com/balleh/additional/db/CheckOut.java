package com.balleh.additional.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.balleh.model.ModelAddress;
import com.balleh.model.ModelPaymentMethod;
import com.balleh.model.ModelShippingMethod;

public class CheckOut extends SQLiteOpenHelper {

    private final static String name = "db_confirm_order";
    private final static int version = 2;
    private final static String SELECT_VALUE_SELECT = "select ";
    private final static String SELECT_VALUE_STAR = "* ";
    private final static String SELECT_VALUE_FROM = "from ";
    private Cursor cursor;

    private final static String TABLE_ACCOUNT_DETAIL = "account_detail";
    private final static String ACCOUNT_FIRST_NAME = "account_first_name";
    private final static String ACCOUNT_LAST_NAME = "account_last_name";
    private final static String ACCOUNT_EMAIL_ID = "account_email_id";
    private final static String ACCOUNT_CITY = "account_city";
    private final static String ACCOUNT_COUNTRY_ID = "account_country_id";
    private final static String ACCOUNT_REGION_ID = "account_region_id";
    private final static String ACCOUNT_REGION_CODE = "account_region_code";
    private final static String ACCOUNT_STREET = "account_street";
    private final static String ACCOUNT_POSTCODE = "account_post_code";
    private final static String ACCOUNT_CUSTOMER_ID = "account_customer_id";
    private final static String ACCOUNT_TELEPHONE = "account_telephone";
    private final static String ACCOUNT_SAME_AS_BILLING = "account_same_as_billing";

    private final static String CREATE_ACCOUNT_DETAIL = "create table " + TABLE_ACCOUNT_DETAIL + "(" + ACCOUNT_FIRST_NAME
            + " text," + ACCOUNT_LAST_NAME + " text," + ACCOUNT_EMAIL_ID + " text," + ACCOUNT_CITY + " text,"
            + ACCOUNT_COUNTRY_ID + " text," + ACCOUNT_REGION_ID + " text," + ACCOUNT_REGION_CODE + " text,"
            + ACCOUNT_STREET + " text," + ACCOUNT_CUSTOMER_ID + " text," + ACCOUNT_TELEPHONE + " text," + ACCOUNT_SAME_AS_BILLING + " text,"
            + ACCOUNT_POSTCODE + " text);";

    private final static String DROP_TABLE_ACCOUNT_DETAIL = "DROP TABLE IF EXISTS " + TABLE_ACCOUNT_DETAIL;

    private final static String TABLE_SHIPPING_TYPE = "shipping_type";
    private final static String SHIPPING_CARRIER_TITLE = "shipping_carrier_title";
    private final static String SHIPPING_CARRIER_CODE = "shipping_carrier_code";
    private final static String SHIPPING_METHOD_TITLE = "shipping_method_title";
    private final static String SHIPPING_METHOD_CODE = "shipping_method_code";
    private final static String SHIPPING_AMOUNT = "shipping_amount";
    private final static String CREATE_SHIPPING_TYPE = "create table " + TABLE_SHIPPING_TYPE + "(" + SHIPPING_CARRIER_TITLE
            + " text," + SHIPPING_CARRIER_CODE + " text," + SHIPPING_METHOD_TITLE + " text," + SHIPPING_METHOD_CODE + " text,"
            + SHIPPING_AMOUNT + " text);";
    private final static String DROP_TABLE_SHIPPING_TYPE = "DROP TABLE IF EXISTS " + TABLE_SHIPPING_TYPE;

    private final static String TABLE_PAYMENT_TYPE = "payment_type";
    private final static String PAYMENT_TITLE = "payment_title";
    private final static String PAYMENT_CODE = "payment_code";
    private final static String PAYMENT_TERMS = "payment_terms";
    private final static String PAYMENT_SORT_ORDER = "payment_sort_order";
    private final static String CREATE_PAYMENT_TYPE = "create table " + TABLE_PAYMENT_TYPE + "(" + PAYMENT_TITLE
            + " text ," + PAYMENT_CODE + " text," + PAYMENT_TERMS + " text," + PAYMENT_SORT_ORDER + " text);";
    private final static String DROP_TABLE_PAYMENT_TYPE = "DROP TABLE IF EXISTS " + TABLE_PAYMENT_TYPE;

    private static CheckOut sInstance;

    public static synchronized CheckOut getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new CheckOut(context.getApplicationContext());
        }
        return sInstance;
    }

    private CheckOut(Context context) {
        super(context, name, null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_SHIPPING_TYPE);
        db.execSQL(CREATE_PAYMENT_TYPE);
        db.execSQL(CREATE_ACCOUNT_DETAIL);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_TABLE_SHIPPING_TYPE);
        db.execSQL(DROP_TABLE_PAYMENT_TYPE);
        db.execSQL(DROP_TABLE_ACCOUNT_DETAIL);
        onCreate(db);
    }

    public void insertAccountDetail(String email_id, ModelAddress address) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ACCOUNT_FIRST_NAME, address.getFirstName());
        contentValues.put(ACCOUNT_LAST_NAME, address.getLastName());
        contentValues.put(ACCOUNT_CITY, address.getCity());
        contentValues.put(ACCOUNT_COUNTRY_ID, address.getCountryId());
        contentValues.put(ACCOUNT_REGION_ID, address.getRegionId());
        contentValues.put(ACCOUNT_REGION_CODE, address.getRegionCode());
        contentValues.put(ACCOUNT_STREET, address.getStreet());
        contentValues.put(ACCOUNT_POSTCODE, address.getPost_code());
        contentValues.put(ACCOUNT_CUSTOMER_ID, address.getCustomerId());
        contentValues.put(ACCOUNT_EMAIL_ID, email_id);
        contentValues.put(ACCOUNT_TELEPHONE, address.getTelephone());
        contentValues.put(ACCOUNT_SAME_AS_BILLING, address.getSaveAddressBook());
        db.insert(TABLE_ACCOUNT_DETAIL, null, contentValues);
    }

    public void insertShippingType(ModelShippingMethod dataSet) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(SHIPPING_CARRIER_TITLE, dataSet.getCarrier_title());
        contentValues.put(SHIPPING_CARRIER_CODE, dataSet.getCarrier_code());
        contentValues.put(SHIPPING_METHOD_CODE, dataSet.getMethod_code());
        contentValues.put(SHIPPING_METHOD_TITLE, dataSet.getMethod_title());
        contentValues.put(SHIPPING_AMOUNT, dataSet.getAmount());
        db.insert(TABLE_SHIPPING_TYPE, null, contentValues);
    }

    public void insertPaymentType(ModelPaymentMethod paymentMethod) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(PAYMENT_TITLE, paymentMethod.getTitle());
        contentValues.put(PAYMENT_CODE, paymentMethod.getCode());
        db.insert(TABLE_PAYMENT_TYPE, null, contentValues);
    }

    public void updateAccountDetail(String email_id, ModelAddress address) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ACCOUNT_FIRST_NAME, address.getFirstName());
        contentValues.put(ACCOUNT_LAST_NAME, address.getLastName());
        contentValues.put(ACCOUNT_CITY, address.getCity());
        contentValues.put(ACCOUNT_COUNTRY_ID, address.getCountryId());
        contentValues.put(ACCOUNT_REGION_ID, address.getRegionId());
        contentValues.put(ACCOUNT_REGION_CODE, address.getRegionCode());
        contentValues.put(ACCOUNT_STREET, address.getStreet());
        contentValues.put(ACCOUNT_POSTCODE, address.getPost_code());
        contentValues.put(ACCOUNT_CUSTOMER_ID, address.getCustomerId());
        contentValues.put(ACCOUNT_EMAIL_ID, email_id);
        contentValues.put(ACCOUNT_TELEPHONE, address.getTelephone());
        contentValues.put(ACCOUNT_SAME_AS_BILLING, address.getSaveAddressBook());
        db.update(TABLE_ACCOUNT_DETAIL, contentValues, null, null);
    }

    public void updateShippingType(ModelShippingMethod dataSet) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(SHIPPING_CARRIER_CODE, dataSet.getCarrier_code());
        contentValues.put(SHIPPING_CARRIER_TITLE, dataSet.getCarrier_title());
        contentValues.put(SHIPPING_METHOD_CODE, dataSet.getMethod_code());
        contentValues.put(SHIPPING_METHOD_TITLE, dataSet.getMethod_title());
        contentValues.put(SHIPPING_AMOUNT, dataSet.getAmount());
        db.update(TABLE_SHIPPING_TYPE, contentValues, null, null);
    }

    public ModelAddress getAccountDetail() {

        ModelAddress dataSet = new ModelAddress();
        SQLiteDatabase db = this.getReadableDatabase();
        String select = SELECT_VALUE_SELECT + SELECT_VALUE_STAR + SELECT_VALUE_FROM + TABLE_ACCOUNT_DETAIL + ";";
        cursor = db.rawQuery(select, null);
        if (cursor != null) {
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {

                dataSet.setFirstName(cursor.getString(cursor.getColumnIndex(ACCOUNT_FIRST_NAME)));
                dataSet.setLastName(cursor.getString(cursor.getColumnIndex(ACCOUNT_LAST_NAME)));
                dataSet.setCity(cursor.getString(cursor.getColumnIndex(ACCOUNT_CITY)));
                dataSet.setCountryId(cursor.getString(cursor.getColumnIndex(ACCOUNT_COUNTRY_ID)));
                dataSet.setRegionId(cursor.getString(cursor.getColumnIndex(ACCOUNT_REGION_ID)));
                dataSet.setRegionCode(cursor.getString(cursor.getColumnIndex(ACCOUNT_REGION_CODE)));
                dataSet.setStreet(cursor.getString(cursor.getColumnIndex(ACCOUNT_STREET)));
                dataSet.setPost_code(cursor.getString(cursor.getColumnIndex(ACCOUNT_POSTCODE)));
                dataSet.setCustomerId(Integer.valueOf(cursor.getString(cursor.getColumnIndex(ACCOUNT_CUSTOMER_ID))));
                dataSet.setEmail_id(cursor.getString(cursor.getColumnIndex(ACCOUNT_EMAIL_ID)));
                dataSet.setTelephone(cursor.getString(cursor.getColumnIndex(ACCOUNT_TELEPHONE)));
                //dataSet.setLastName(cursor.getString(cursor.getColumnIndex(ACCOUNT_SAME_AS_BILLING)));
            }
            cursor.close();
            return dataSet;
        }
        return null;
    }

    public ModelShippingMethod getShippingType() {

        ModelShippingMethod dataSet = new ModelShippingMethod();
        SQLiteDatabase db = this.getReadableDatabase();
        String select = SELECT_VALUE_SELECT + SELECT_VALUE_STAR + SELECT_VALUE_FROM + TABLE_SHIPPING_TYPE + ";";
        cursor = db.rawQuery(select, null);
        if (cursor != null) {
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                dataSet.setCarrier_title(cursor.getString(cursor.getColumnIndex(SHIPPING_CARRIER_TITLE)));
                dataSet.setCarrier_code(cursor.getString(cursor.getColumnIndex(SHIPPING_CARRIER_CODE)));
                dataSet.setMethod_title(cursor.getString(cursor.getColumnIndex(SHIPPING_METHOD_TITLE)));
                dataSet.setMethod_code(cursor.getString(cursor.getColumnIndex(SHIPPING_METHOD_CODE)));
                dataSet.setAmount(cursor.getString(cursor.getColumnIndex(SHIPPING_AMOUNT)));
            }
            cursor.close();
            return dataSet;
        }
        return null;
    }

    public ModelPaymentMethod getPaymentType() {

        ModelPaymentMethod list = new ModelPaymentMethod();
        SQLiteDatabase db = this.getReadableDatabase();
        String select = SELECT_VALUE_SELECT + SELECT_VALUE_STAR + SELECT_VALUE_FROM + TABLE_PAYMENT_TYPE + ";";
        cursor = db.rawQuery(select, null);
        if (cursor != null) {
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                list.setTitle(cursor.getString(cursor.getColumnIndex(PAYMENT_TITLE)));
                list.setCode(cursor.getString(cursor.getColumnIndex(PAYMENT_CODE)));
            }
            cursor.close();
            return list;
        }
        return null;
    }

    public void updatePaymentType(ModelPaymentMethod modelPaymentMethod) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(PAYMENT_TITLE, modelPaymentMethod.getTitle());
        contentValues.put(PAYMENT_CODE, modelPaymentMethod.getCode());
        db.update(TABLE_PAYMENT_TYPE, contentValues, null, null);
    }

    public int getSizeAccount() {
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        return (int) DatabaseUtils.queryNumEntries(sqLiteDatabase, TABLE_ACCOUNT_DETAIL);
    }

    public int getSizePayment() {
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        return (int) DatabaseUtils.queryNumEntries(sqLiteDatabase, TABLE_PAYMENT_TYPE);
    }

    public int getSizeShipping() {
        SQLiteDatabase db = this.getReadableDatabase();
        return (int) DatabaseUtils.queryNumEntries(db, TABLE_SHIPPING_TYPE);
    }

    public void deleteAddressDetail() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_ACCOUNT_DETAIL, null, null);
    }

    public void deleteShippingType() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_SHIPPING_TYPE, null, null);
    }

    public void deletePaymentType() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_PAYMENT_TYPE, null, null);
    }

    @Override
    protected void finalize() throws Throwable {
        this.close();
        super.finalize();
    }
}
