package com.balleh.additional;

import android.graphics.Typeface;
import android.support.design.widget.TextInputLayout;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

/**
 * Created by user on 9/25/2018.
 * Font style Handler
 */

public class Constant {

    public static int EN = 1, AR = 2, BOLD = 3, LIGHT = 4;

    public static int C_TEXT_VIEW = 1, C_BUTTON = 2, C_RADIO_BUTTON = 3, C_CHECKBOX = 4, C_EDIT_TEXT = 5, C_TEXT_INPUT_LAYOUT = 6;

    /*
  Parameter    o_SetFont - Which need to change font style,    type - text style,    which - Object type
  Object types:    1 - TextView,  2 - Button, 3 - RadioButton, 4 - CheckBox,5 - EditText, 6 - TextInputLayout*/
    public static void SetFontStyle(Object o_SetFont, int type, int which) {

        if (ConstantFields.currentLanguage().equals("en")) {
            type = Constant.EN;
        } else {
            type = Constant.AR;
        }

        Typeface FontStyle;
        if (type == EN) {
            FontStyle = Typeface.createFromAsset(ApplicationContext.getInstance().getAssets(), "fonts/english.ttf");
        } else {
            FontStyle = Typeface.createFromAsset(ApplicationContext.getInstance().getAssets(), "fonts/arabic.otf");
        }

        if (which == 1) {
            TextView tv_Font = (TextView) o_SetFont;
            tv_Font.setTypeface(FontStyle);
        }

        if (which == 2) {
            Button btn_Font = (Button) o_SetFont;
            btn_Font.setTypeface(FontStyle);
        }

        if (which == 3) {
            RadioButton rb_Font = (RadioButton) o_SetFont;
            rb_Font.setTypeface(FontStyle);
        }

        if (which == 4) {
            CheckBox cb_Font = (CheckBox) o_SetFont;
            cb_Font.setTypeface(FontStyle);
        }

        if (which == 5) {
            EditText et_Font = (EditText) o_SetFont;
            et_Font.setTypeface(FontStyle);
        }
        if (which == 6) {
            TextInputLayout et_Font = (TextInputLayout) o_SetFont;
            et_Font.setTypeface(FontStyle);
        }
    }
}
