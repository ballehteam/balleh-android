package com.balleh.model;

import java.util.ArrayList;

public class ModelCountry {

    private String name, id,code;
    private ArrayList<ModelState> stateList;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public ArrayList<ModelState> getStateList() {
        return stateList;
    }

    public void setStateList(ArrayList<ModelState> stateList) {
        this.stateList = stateList;
    }
}
