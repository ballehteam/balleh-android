package com.balleh.model;

/**
 * Created by user on 7/25/2018.
 * Filter Value
 */

public class ModelFilterValue {

    private String ValueTitle,ValueId,ParentCode;
    private boolean select;

    public String getValueTitle() {
        return ValueTitle;
    }

    public void setValueTitle(String valueTitle) {
        ValueTitle = valueTitle;
    }

    public String getValueId() {
        return ValueId;
    }

    public void setValueId(String valueId) {
        ValueId = valueId;
    }

    public boolean isSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }

    public String getParentCode() {
        return ParentCode;
    }

    public void setParentCode(String parentCode) {
        ParentCode = parentCode;
    }
}
