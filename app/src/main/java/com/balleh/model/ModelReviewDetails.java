package com.balleh.model;

import java.util.ArrayList;

/**
 * Created by user on 7/6/2018.
 * Review Detail
 */

public class ModelReviewDetails {

    private String title, name, description, date;
    private int review;
    private ArrayList<String[]> getRatingList = new ArrayList<>();

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public ArrayList<String[]> getGetRatingList() {
        return getRatingList;
    }

    public void setGetRatingList(ArrayList<String[]> getRatingList) {
        this.getRatingList = getRatingList;
    }

    public int getReview() {
        return review;
    }

    public void setReview(int review) {
        this.review = review;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
