package com.balleh.model;

import java.util.ArrayList;

public class ModelHomeMenuCategoryList {

    private ModelHomeMenuCategory modelHomeMenuCategory;
    private ArrayList<ModelHomeMenuCategory> homeMenuCategories;

    public ArrayList<ModelHomeMenuCategory> getHomeMenuCategories() {
        return homeMenuCategories;
    }

    public void setHomeMenuCategories(ArrayList<ModelHomeMenuCategory> homeMenuCategories) {
        this.homeMenuCategories = homeMenuCategories;
    }

    public ModelHomeMenuCategory getHomeMenuCategory() {
        return modelHomeMenuCategory;
    }

    public void setHomeMenuCategory(ModelHomeMenuCategory modelHomeMenuCategory) {
        this.modelHomeMenuCategory = modelHomeMenuCategory;
    }
}
