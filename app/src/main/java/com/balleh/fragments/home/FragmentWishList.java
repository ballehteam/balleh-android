package com.balleh.fragments.home;

import android.app.Activity;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.balleh.R;
import com.balleh.additional.AppLanguageSupport;
import com.balleh.additional.Constant;
import com.balleh.additional.ConstantDataParser;
import com.balleh.additional.ConstantFields;
import com.balleh.additional.ImageLoader;
import com.balleh.additional.db.wishlist.WishListDBHandler;
import com.balleh.additional.db.wishlist.WishListDao;
import com.balleh.additional.db.wishlist.WishListRoom;
import com.balleh.additional.instentTransfer.MenuHandler;
import com.balleh.model.ModelProductDetailList;

import java.util.ArrayList;
import java.util.List;

public class FragmentWishList extends Fragment {

    private View v_WishList;
    private Activity activity;
    private ArrayList<ModelProductDetailList> mProductList = new ArrayList<>();
    private WishListDao wishListDao;
    private MenuHandler menuHandler;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v_WishList = inflater.inflate(R.layout.fragment_wish_list, container, false);
        load();
        return v_WishList;
    }

    private void load() {
        RecyclerView rc_WishListLister = v_WishList.findViewById(R.id.rc_wish_list_container);
        rc_WishListLister.setLayoutManager(new LinearLayoutManager(activity));

        //    mProductList = WishList.getInstance(activity).getWishListProductDetail();
        WishListDBHandler wishListDBHandler = Room.databaseBuilder(activity, WishListDBHandler.class, "WishList")
                .allowMainThreadQueries().build();
        wishListDao = wishListDBHandler.getWishList();

        List<WishListRoom> wishListRooms = wishListDao.getWishList();
        for (int i = 0; i < wishListRooms.size(); i++) {
            Log.e("load: ", wishListRooms.get(i).getProductDetails());

            ModelProductDetailList modelProductDetail = ConstantDataParser.getWishListProduct(wishListRooms.get(i).getProductDetails());
            if (modelProductDetail != null) {
                mProductList.add(modelProductDetail);
            }
        }
        rc_WishListLister.setAdapter(new WishListAdapter());
    }

    class WishListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            RecyclerView.ViewHolder viewHolder;

            if (viewType == 1) {
                viewHolder = new WishListViewHolder(LayoutInflater.from(activity).inflate(R.layout.rc_row_wish_list, parent, false));
            } else {
                viewHolder = new EmptyProductList(LayoutInflater.from(activity).inflate(R.layout.rc_row_empty, parent, false));
            }

            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
            if (holder.getItemViewType() == 1) {
                WishListViewHolder wishListHolder = (WishListViewHolder) holder;

                if (ConstantFields.currentLanguage().equals("ar")) {
                    if (!mProductList.get(position).getAr_title().equals("a")) {
                        wishListHolder.atv_ProductTitleList.setText(mProductList.get(position).getAr_title());
                    } else {
                        wishListHolder.atv_ProductTitleList.setText(mProductList.get(position).getName());
                    }
                } else {
                    if (!mProductList.get(position).getEn_title().equals("a")) {
                        wishListHolder.atv_ProductTitleList.setText(mProductList.get(position).getEn_title());
                    } else {
                        wishListHolder.atv_ProductTitleList.setText(mProductList.get(position).getName());
                    }
                }

                if (mProductList.get(position).getAttribute_result() != null) {
                    if (mProductList.get(position).getAttribute_result().size() > 0) {
                        for (int i = 0; i < mProductList.get(position).getAttribute_result().size(); i++) {
                            if (mProductList.get(position).getAttribute_result().get(i)[0].trim().toLowerCase().equals("small_image")) {
                                ImageLoader.glide_image_loader(ConstantFields.ImageURL + mProductList.get(position).getAttribute_result().get(i)[1],
                                        wishListHolder.iv_ProductImageList, "Original");
                            }
                        }
                    }
                }

                wishListHolder.atv_ProductPriceList.setText(String.valueOf("SAR " + mProductList.get(position).getPrice()));
                wishListHolder.ib_ProductRemove.setOnClickListener(view -> {
                    String message = mProductList.get(position).getName() + " " + getString(R.string.wish_list_remove);
                    showToast(message);
                    //  WishList.getInstance(activity.getApplicationContext()).remove_from_wish_list(mProductList.get(position).getSku());
                    wishListDao.deleteProduct(mProductList.get(position).getSku());
                    mProductList.remove(position);
                    notifyDataSetChanged();
                });

            } else {
                EmptyProductList empty_view = (EmptyProductList) holder;
                empty_view.atv_EmptyMessage.setText(activity.getString(R.string.wish_list_empty_text));
            }
        }

        @Override
        public int getItemCount() {
            if (mProductList != null) {
                if (mProductList.size() > 0) {
                    return mProductList.size();
                } else {
                    return 1;
                }
            } else {
                return 1;
            }
        }

        @Override
        public int getItemViewType(int position) {
            if (mProductList != null) {
                if (mProductList.size() > 0) {
                    return 1;
                } else {
                    return 2;
                }
            } else {
                return 2;
            }
        }

        private class WishListViewHolder extends RecyclerView.ViewHolder {
            ImageView iv_ProductImageList;
            AppCompatTextView atv_ProductTitleList, atv_ProductPriceList;
            ImageButton ib_ProductRemove;

            WishListViewHolder(View view) {
                super(view);
                iv_ProductImageList = view.findViewById(R.id.iv_product_image_wish_list);
                atv_ProductTitleList = view.findViewById(R.id.atv_product_title_wish_list);
                atv_ProductPriceList = view.findViewById(R.id.atv_product_price_wish_list);
                ib_ProductRemove = view.findViewById(R.id.ib_product_remove_wish_list);
                fontSetup(atv_ProductPriceList, Constant.LIGHT, Constant.C_TEXT_VIEW);
                fontSetup(atv_ProductTitleList, Constant.LIGHT, Constant.C_TEXT_VIEW);
            }
        }

        private class EmptyProductList extends RecyclerView.ViewHolder {
            AppCompatTextView atv_EmptyMessage;

            EmptyProductList(View view) {
                super(view);
                atv_EmptyMessage = view.findViewById(R.id.atv_empty_message);
                fontSetup(atv_EmptyMessage, Constant.LIGHT, Constant.C_TEXT_VIEW);
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(AppLanguageSupport.onAttach(context));
        activity = (Activity) context;
        menuHandler = (MenuHandler) context;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        menuHandler.changeBottomView(3);
    }

    private void showToast(String message) {
        ConstantFields.CustomToast(message, activity);
    }

    private void fontSetup(Object o_SetFont, int type, int which) {
        Constant.SetFontStyle(o_SetFont, type, which);
    }

}
