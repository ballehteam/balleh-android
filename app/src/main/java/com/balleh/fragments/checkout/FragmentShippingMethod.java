package com.balleh.fragments.checkout;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RadioButton;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.balleh.R;
import com.balleh.additional.AppLanguageSupport;
import com.balleh.additional.ApplicationContext;
import com.balleh.additional.Constant;
import com.balleh.additional.ConstantDataParser;
import com.balleh.additional.ConstantDataSaver;
import com.balleh.additional.ConstantFields;
import com.balleh.additional.db.CheckOut;
import com.balleh.additional.instentTransfer.CheckOutHandler;
import com.balleh.fragments.DialogLoader;
import com.balleh.model.ModelAddress;
import com.balleh.model.ModelShippingMethod;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FragmentShippingMethod extends Fragment {

    private View v_ShippingContainer;
    private AppCompatTextView atv_PaymentContinue, atv_ShippingTitle;
    private Activity activity;
    private CheckOutHandler checkOutHandler;
    private RecyclerView rc_ShippingList;
    private boolean isSelected = false;
    private String type;
    private ImageButton ib_ToAddress;
    private DialogLoader dialogLoader;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mTokenTag).equals("Empty")) {
            type = "Customer";
        } else {
            type = "Guest";
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v_ShippingContainer = inflater.inflate(R.layout.fragment_checkout_shipping_method, container, false);
        load();
        return v_ShippingContainer;
    }

    private void load() {

        atv_PaymentContinue = v_ShippingContainer.findViewById(R.id.atv_to_payment_method);
        rc_ShippingList = v_ShippingContainer.findViewById(R.id.rc_shipping_list);
        ib_ToAddress = v_ShippingContainer.findViewById(R.id.ib_delivery_type_delivery_detail);
        atv_ShippingTitle = v_ShippingContainer.findViewById(R.id.atv_shipping_title);

        dialogLoader = new DialogLoader();
        dialogLoader.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        dialogLoader.setCancelable(false);

        if (type.equals("Customer")) {
            getCustomerShippingMethod();
        } else {
            getGuestShippingMethod();
        }
        rc_ShippingList.setLayoutManager(new LinearLayoutManager(activity));

        ib_ToAddress.setOnClickListener(view -> checkOutHandler.backPressed("ToAddress", "Shipping"));
        fontSetup();
    }

    @Override
    public void onResume() {
        super.onResume();
        isSelected = false;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(AppLanguageSupport.onAttach(context));
        activity = (Activity) context;
        checkOutHandler = (CheckOutHandler) context;
    }

    private void getCustomerShippingMethod() {

        try {
            ModelAddress modelAddress = CheckOut.getInstance(activity).getAccountDetail();
            if (modelAddress != null) {
                JSONArray jsonArray = new JSONArray();
                jsonArray.put(modelAddress.getStreet());
                JSONObject jsonObjectAddress = new JSONObject();
                jsonObjectAddress.put("firstname", modelAddress.getFirstName());
                jsonObjectAddress.put("lastname", modelAddress.getLastName());
                jsonObjectAddress.put("city", modelAddress.getCity());
                jsonObjectAddress.put("country_id", modelAddress.getCountryId());
                jsonObjectAddress.put("region_id", modelAddress.getRegionId());
                jsonObjectAddress.put("region_code", modelAddress.getRegionCode());
                jsonObjectAddress.put("region", modelAddress.getRegionCode());
                jsonObjectAddress.put("street", jsonArray);
                jsonObjectAddress.put("postcode", modelAddress.getPost_code());
                jsonObjectAddress.put("customer_id", modelAddress.getCustomerId());
                jsonObjectAddress.put("email", modelAddress.getEmail_id());
                jsonObjectAddress.put("telephone", modelAddress.getTelephone());
                jsonObjectAddress.put("same_as_billing", "1");

                final JSONObject jsonObject = new JSONObject();
                jsonObject.put("address", jsonObjectAddress);

                Log.e("getShippingMethod: ", jsonObject.toString());
                dialogHandler(1);

                Log.e("CustomerShipMethod: ", ConstantFields.getCustomerShippintMethod(ConstantFields.currentLanguage().equals("en")));

                if (checkOutHandler.networkChecker()) {
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, ConstantFields.getCustomerShippintMethod(ConstantFields.currentLanguage().equals("en")),
                            response -> {
                                dialogHandler(2);

                                Log.e("onResponse: ", response + "");

                                final ArrayList<ModelShippingMethod> shippingList = ConstantDataParser.getShippingMethod(response);

                                ShippingAdapter shippingAdapter = new ShippingAdapter(activity, shippingList);
                                rc_ShippingList.setAdapter(shippingAdapter);

                                if (shippingList != null) {
                                    if (shippingList.size() > 0) {
                                        atv_PaymentContinue.setOnClickListener(v -> {
                                            if (isSelected) {
                                                checkOutHandler.loadPaymentMethodList();
                                            } else {
                                                ConstantFields.CustomToast(getString(R.string.shipping_error), activity);
                                            }
                                        });
                                    }
                                }
                            }, error -> {
                        dialogHandler(2);
                        try {
                            if (error.networkResponse != null) {
                                String responseBody = new String(error.networkResponse.data, "utf-8");
                                JSONObject jsonObject1 = new JSONObject(responseBody);
                                Log.e("onErrorResponse: ", jsonObject1.toString());
                            }
                        } catch (Exception e) {
                            Log.e("onErrorResponse: ", e.toString());
                        }
                    }) {


                        @Override
                        public byte[] getBody() {
                            return jsonObject.toString().getBytes();
                        }

                        @Override
                        public Map<String, String> getHeaders() {
                            Map<String, String> params = new HashMap<>();
                            params.put("Content-Type", "application/json; charset=utf-8");
                            params.put("Authorization", "Bearer " + ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(),
                                    ConstantFields.mTokenTag));
                            return params;
                        }
                    };

                    stringRequest.setShouldCache(false);
                    ApplicationContext.getInstance().addToRequestQueue(stringRequest, "ShippingMethod");
                }
            }

        } catch (Exception e) {
            Log.e("getShippingMethod: ", e.toString());
        }
    }

    private void getGuestShippingMethod() {

        try {
            ModelAddress modelAddress = CheckOut.getInstance(activity).getAccountDetail();
            if (modelAddress != null) {
                JSONArray jsonArray = new JSONArray();
                jsonArray.put(modelAddress.getStreet());
                JSONObject jsonObjectAddress = new JSONObject();
                jsonObjectAddress.put("firstname", modelAddress.getFirstName());
                jsonObjectAddress.put("lastname", modelAddress.getLastName());
                jsonObjectAddress.put("city", modelAddress.getCity());
                jsonObjectAddress.put("country_id", modelAddress.getCountryId());
                jsonObjectAddress.put("region_id", modelAddress.getRegionId());
                jsonObjectAddress.put("region_code", modelAddress.getRegionCode());
                jsonObjectAddress.put("region", modelAddress.getRegionCode());
                jsonObjectAddress.put("street", jsonArray);
                jsonObjectAddress.put("postcode", modelAddress.getPost_code());
                jsonObjectAddress.put("email", modelAddress.getEmail_id());
                jsonObjectAddress.put("telephone", modelAddress.getTelephone());
                jsonObjectAddress.put("same_as_billing", "1");

                final JSONObject jsonObject = new JSONObject();
                jsonObject.put("address", jsonObjectAddress);

                Log.e("getShippingMethod: ", jsonObject.toString());
                dialogHandler(1);

                if (checkOutHandler.networkChecker()) {
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, ConstantFields.getGuestShippingMethod(ConstantDataSaver.
                            mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mGuestCartIdTag), ConstantFields.currentLanguage().equals("en")),
                            response -> {
                                dialogHandler(2);

                                Log.e("onResponse: ", response + "");

                                final ArrayList<ModelShippingMethod> shippingList = ConstantDataParser.getShippingMethod(response);

                                ShippingAdapter shippingAdapter = new ShippingAdapter(activity, shippingList);
                                rc_ShippingList.setAdapter(shippingAdapter);

                                if (shippingList != null) {
                                    if (shippingList.size() > 0) {
                                        atv_PaymentContinue.setOnClickListener(v -> {
                                            if (isSelected) {
                                                checkOutHandler.loadPaymentMethodList();
                                            } else {
                                                ConstantFields.CustomToast(getString(R.string.shipping_error), activity);
                                            }
                                        });
                                    }
                                }
                            }, error -> {
                        dialogHandler(2);
                        try {
                            if (error.networkResponse != null) {
                                String responseBody = new String(error.networkResponse.data, "utf-8");
                                JSONObject jsonObject1 = new JSONObject(responseBody);
                                Log.e("onErrorResponse: ", jsonObject1.toString());
                            }
                        } catch (Exception e) {
                            Log.e("onErrorResponse: ", e.toString());
                        }
                    }) {


                        @Override
                        public byte[] getBody() {
                            return jsonObject.toString().getBytes();
                        }

                        @Override
                        public Map<String, String> getHeaders() {
                            Map<String, String> params = new HashMap<>();
                            params.put("Content-Type", "application/json; charset=utf-8");
                            params.put("Authorization", "Bearer " + ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(),
                                    ConstantFields.mTokenTag));
                            return params;
                        }
                    };

                    stringRequest.setShouldCache(false);
                    ApplicationContext.getInstance().addToRequestQueue(stringRequest, "ShippingMethod");
                }
            }

        } catch (Exception e) {
            Log.e("getShippingMethod: ", e.toString());
        }
    }

    class ShippingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private Activity activity;
        private ArrayList<ModelShippingMethod> shippingMethodList;

        ShippingAdapter(Activity activity, ArrayList<ModelShippingMethod> shippingMethodList) {
            this.activity = activity;
            this.shippingMethodList = shippingMethodList;
        }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            RecyclerView.ViewHolder holder;
            if (viewType == 1) {
                holder = new ShippingList(LayoutInflater.from(activity).inflate(R.layout.rc_row_shipping_row, parent, false));
            } else {
                holder = new EmptyShippingList(LayoutInflater.from(activity).inflate(R.layout.rc_row_empty, parent, false));
            }
            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            if (holder.getItemViewType() == 1) {
                ShippingList shippingList = (ShippingList) holder;
                shippingList.atv_ShippingTitle.setText(shippingMethodList.get(position).getCarrier_title());

                String methodTitle = shippingMethodList.get(position).getMethod_title() + " ( " + shippingMethodList.get(position).getAmount() + " )";

                shippingList.rb_ShippingMethod.setText(methodTitle);
            } else {
                EmptyShippingList emptyShippingList = (EmptyShippingList) holder;
                emptyShippingList.atv_EmptyMessage.setText(activity.getString(R.string.shipping_empty_error));
            }
        }

        @Override
        public int getItemCount() {
            if (shippingMethodList != null) {
                if (shippingMethodList.size() > 0) {
                    return shippingMethodList.size();
                } else {
                    return 1;
                }
            } else {
                return 1;
            }
        }

        @Override
        public int getItemViewType(int position) {
            if (shippingMethodList != null) {
                if (shippingMethodList.size() > 0) {
                    return 1;
                } else {
                    return 2;
                }
            } else {
                return 2;
            }
        }

        private class ShippingList extends RecyclerView.ViewHolder implements View.OnClickListener {

            AppCompatTextView atv_ShippingTitle;
            RadioButton rb_ShippingMethod;

            ShippingList(View view) {
                super(view);
                atv_ShippingTitle = view.findViewById(R.id.atv_shipping_title);
                rb_ShippingMethod = view.findViewById(R.id.rb_shipping_method);
                rb_ShippingMethod.setOnClickListener(this);
                fontSetup(atv_ShippingTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);
                fontSetup(rb_ShippingMethod, Constant.LIGHT, Constant.C_RADIO_BUTTON);
            }

            @Override
            public void onClick(View view) {
                int id = view.getId();
                switch (id) {
                    case R.id.rb_shipping_method:
                        if (getAdapterPosition() != -1) {
                            isSelected = true;
                            ModelShippingMethod shippingMethod = new ModelShippingMethod();

                            if (CheckOut.getInstance(activity).getSizeShipping() == 0) {
                                shippingMethod.setCarrier_title(shippingMethodList.get(getAdapterPosition()).getCarrier_title());
                                shippingMethod.setCarrier_code(shippingMethodList.get(getAdapterPosition()).getCarrier_code());
                                shippingMethod.setMethod_code(shippingMethodList.get(getAdapterPosition()).getMethod_code());
                                shippingMethod.setMethod_title(shippingMethodList.get(getAdapterPosition()).getMethod_title());
                                shippingMethod.setAmount(shippingMethodList.get(getAdapterPosition()).getAmount());
                                CheckOut.getInstance(activity).insertShippingType(shippingMethod);
                            } else {
                                shippingMethod.setCarrier_title(shippingMethodList.get(getAdapterPosition()).getCarrier_title());
                                shippingMethod.setCarrier_code(shippingMethodList.get(getAdapterPosition()).getCarrier_code());
                                shippingMethod.setMethod_code(shippingMethodList.get(getAdapterPosition()).getMethod_code());
                                shippingMethod.setMethod_title(shippingMethodList.get(getAdapterPosition()).getMethod_title());
                                shippingMethod.setAmount(shippingMethodList.get(getAdapterPosition()).getAmount());
                                CheckOut.getInstance(activity).updateShippingType(shippingMethod);
                            }
                        }
                        break;
                }
            }
        }

        private class EmptyShippingList extends RecyclerView.ViewHolder {
            AppCompatTextView atv_EmptyMessage;

            EmptyShippingList(View view) {
                super(view);
                atv_EmptyMessage = view.findViewById(R.id.atv_empty_message);
            }
        }
    }

    private void dialogHandler(int type) {
        if (type == 1) {
            dialogLoader.show(getChildFragmentManager(), "DialogLoader");
        } else {
            dialogLoader.dismiss();
        }
    }

    private void fontSetup() {
        fontSetup(atv_ShippingTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_PaymentContinue, Constant.LIGHT, Constant.C_TEXT_VIEW);
    }

    private void fontSetup(Object o_SetFont, int type, int which) {
        Constant.SetFontStyle(o_SetFont, type, which);
    }

}
