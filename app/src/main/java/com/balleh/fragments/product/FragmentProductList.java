package com.balleh.fragments.product;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.balleh.R;
import com.balleh.adapter.ProductListAdapter;
import com.balleh.adapter.ProductListSubCategoryList;
import com.balleh.additional.AppLanguageSupport;
import com.balleh.additional.ApplicationContext;
import com.balleh.additional.Constant;
import com.balleh.additional.ConstantDataParser;
import com.balleh.additional.ConstantFields;
import com.balleh.additional.ImageLoader;
import com.balleh.additional.instentTransfer.MenuHandler;
import com.balleh.additional.instentTransfer.ProductListHelper;
import com.balleh.model.ModelCategoryDetail;
import com.balleh.model.ModelHomeMenuCategory;
import com.balleh.model.ModelProductDetailList;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FragmentProductList extends Fragment implements ProductListHelper, View.OnClickListener {

    private View v_ProductListContainer;
    private ImageView iv_CategoryBanner;
    private AppCompatTextView atv_ParentCategoryTitle;
    private ImageButton ib_SubCategoryProductList;
    private ImageButton ib_ViewTypeProductList;
    private RecyclerView rc_ProductList, rc_SubcategoryList;
    private DrawerLayout dl_SubCategoryList;
    private int mViewType = 1, mSubcategoryVisibleStatus = 1, mCategoryId = 0, page = 1;
    private String mLoadType = "default", filter = "";
    private Activity activity;
    private ProgressBar pb_ProductListLoader;
    private ProductListAdapter productListAdapter;
    private MenuHandler menuHandler;
    private ProductListHelper productListHelper;
    private ModelCategoryDetail modelCategoryDetail;
    private ArrayList<ModelProductDetailList> modelProductDetailLists = new ArrayList<>();
    private PopupWindow mSortingList;

    public FragmentProductList() {
    }

    public static FragmentProductList getInstance(int categoryId, String type, String filter) {
        FragmentProductList fragmentProductList = new FragmentProductList();
        Bundle bundle = new Bundle();
        bundle.putInt("CategoryId", categoryId);
        bundle.putString("LoadType", type);
        bundle.putString("Filter", filter);
        fragmentProductList.setArguments(bundle);
        return fragmentProductList;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mCategoryId = getArguments().getInt("CategoryId");
            mLoadType = getArguments().getString("LoadType");
            filter = getArguments().getString("Filter");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v_ProductListContainer = inflater.inflate(R.layout.fragment_product_list, container, false);
        load();
        return v_ProductListContainer;
    }

    private void load() {

        ImageButton ib_SortingProductList = v_ProductListContainer.findViewById(R.id.ib_sorting_product_list);

        iv_CategoryBanner = v_ProductListContainer.findViewById(R.id.iv_category_image_product_list);

        ImageButton ib_FilterProductList = v_ProductListContainer.findViewById(R.id.ib_filter_product_list);
        ib_SubCategoryProductList = v_ProductListContainer.findViewById(R.id.ib_sub_category_product_list);
        ib_ViewTypeProductList = v_ProductListContainer.findViewById(R.id.ib_view_type_product_list);

        atv_ParentCategoryTitle = v_ProductListContainer.findViewById(R.id.atv_sub_category_list_parent_title);

        rc_ProductList = v_ProductListContainer.findViewById(R.id.rc_product_lister_product_list);
        rc_SubcategoryList = v_ProductListContainer.findViewById(R.id.rc_sub_category_product_list);

        dl_SubCategoryList = v_ProductListContainer.findViewById(R.id.dl_product_list);

        pb_ProductListLoader = v_ProductListContainer.findViewById(R.id.pb_product_list_loader);

        rc_SubcategoryList.setLayoutManager(new LinearLayoutManager(activity));

        loadProductListViewType("1");
        loadCategory();

        ib_FilterProductList.setOnClickListener(v -> menuHandler.loadFilter(mCategoryId));

        ib_SubCategoryProductList.setOnClickListener(view -> {
            if (mSubcategoryVisibleStatus == 1) {
                mSubcategoryVisibleStatus = 2;
                dl_SubCategoryList.openDrawer(Gravity.END);
            } else {
                mSubcategoryVisibleStatus = 1;
                dl_SubCategoryList.closeDrawers();
            }
        });

        ib_ViewTypeProductList.setOnClickListener(view -> {
            if (mViewType == 1) {
                mViewType = 2;
                ib_ViewTypeProductList.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_view_list_black_24dp));
                loadProductListViewType("2");
            } else {
                mViewType = 1;
                ib_ViewTypeProductList.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_view_module_black_24dp));
                loadProductListViewType("1");
            }
        });

        ib_SubCategoryProductList.setVisibility(View.GONE);

        dl_SubCategoryList.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        ib_SortingProductList.setOnClickListener(this::sorting);

        fontSetup();
    }

    private void sorting(View v) {

        mSortingList = new PopupWindow(activity);
        @SuppressLint("InflateParams") View view = getLayoutInflater().inflate(R.layout.pw_sorting_list, null);
        mSortingList.setContentView(view);

        AppCompatTextView atv_NameAZ, atv_NameZA, atv_PriceLH, atv_PriceHL,
                atv_PositionLH, atv_PositionHL;

        atv_NameAZ = view.findViewById(R.id.atv_name_az_sorting);
        atv_NameZA = view.findViewById(R.id.atv_name_za_sorting);
        atv_PriceLH = view.findViewById(R.id.atv_price_lh_sorting);
        atv_PriceHL = view.findViewById(R.id.atv_price_hl_sorting);
        atv_PositionLH = view.findViewById(R.id.atv_position_az_sorting);
        atv_PositionHL = view.findViewById(R.id.atv_position_za_sorting);

        mSortingList.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        mSortingList.setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
        mSortingList.setOutsideTouchable(true);
        mSortingList.setFocusable(true);
        mSortingList.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mSortingList.showAtLocation(v, Gravity.CENTER, 0, 0);

        atv_NameAZ.setOnClickListener(this);
        atv_NameZA.setOnClickListener(this);
        atv_PriceLH.setOnClickListener(this);
        atv_PriceHL.setOnClickListener(this);
        atv_PositionLH.setOnClickListener(this);
        atv_PositionHL.setOnClickListener(this);

        fontSetup(atv_NameAZ, Constant.LIGHT);
        fontSetup(atv_NameZA, Constant.LIGHT);
        fontSetup(atv_PriceLH, Constant.LIGHT);
        fontSetup(atv_PriceHL, Constant.LIGHT);
        fontSetup(atv_PositionLH, Constant.LIGHT);
        fontSetup(atv_PositionHL, Constant.LIGHT);

    }

    private void loadProductListViewType(String type) {
        if (type.equals("1")) {
            rc_ProductList.setLayoutManager(new GridLayoutManager(activity, 2));
        } else {
            rc_ProductList.setLayoutManager(new LinearLayoutManager(activity));
        }
        if (productListAdapter != null)
            productListAdapter.notifyDataSetChanged();
    }

    private void loadCategory() {

        if (dl_SubCategoryList != null) {
            dl_SubCategoryList.closeDrawers();
        }

        if (menuHandler.networkError()) {
            pb_ProductListLoader.setVisibility(View.VISIBLE);

            StringRequest stringRequest = new StringRequest(Request.Method.GET, ConstantFields.getCategoryDetail(mCategoryId, ConstantFields.currentLanguage().equals("en")),
                    response -> {
                        pb_ProductListLoader.setVisibility(View.GONE);

                        modelCategoryDetail = ConstantDataParser.getCategoryDetail(response);

                        if (modelCategoryDetail != null) {

                            if (modelCategoryDetail.getParentCategoryName() != null)
                                if (modelCategoryDetail.getParentCategoryName().length() > 0)
                                    atv_ParentCategoryTitle.setText(modelCategoryDetail.getParentCategoryName());

                            if (modelCategoryDetail.getSubcategory() != null) {
                                if (modelCategoryDetail.getSubcategory().length() > 0) {
                                    loadSubCategoryProductList(mCategoryId + "," + modelCategoryDetail.getSubcategory());
                                } else {
                                    loadProductList();
                                }
                            } else {
                                loadProductList();
                            }

                            if (modelCategoryDetail.getCustomAttributes() != null) {
                                if (modelCategoryDetail.getCustomAttributes().size() > 0) {
                                    for (int i = 0; i < modelCategoryDetail.getCustomAttributes().size(); i++) {
                                        if (modelCategoryDetail.getCustomAttributes().get(i)[0].trim().toLowerCase().equals("image")) {
                                            ImageLoader.glide_image_loader(ConstantFields.ImageURL
                                                            + modelCategoryDetail.getCustomAttributes().get(i)[1],
                                                    iv_CategoryBanner, "Original");
                                        }
                                    }
                                }
                            }

                            if (menuHandler.networkError())
                                loadCategoryList();

                        }
                    }, error -> pb_ProductListLoader.setVisibility(View.GONE)) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<>();
                    params.put("Content-Type", "application/json");
                    params.put("Authorization", ConstantFields.getGuestToken());
                    return params;
                }
            };

            stringRequest.setShouldCache(false);
            ApplicationContext.getInstance().addToRequestQueue(stringRequest, "CategoryDetail");
        }

    }

    private void loadCategoryList() {

        pb_ProductListLoader.setVisibility(View.VISIBLE);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, ConstantFields.getSubCategoryList(mCategoryId, ConstantFields.currentLanguage().equals("en")),
                response -> {
                    pb_ProductListLoader.setVisibility(View.GONE);
                    ArrayList<ModelHomeMenuCategory> subCategoryList = ConstantDataParser.getSubCategoryList(response);
                    if (subCategoryList != null) {
                        if (subCategoryList.size() > 0) {
                            rc_SubcategoryList.setAdapter(new ProductListSubCategoryList(activity, subCategoryList, productListHelper));
                            //atv_SubCategoryProductList.setVisibility(View.VISIBLE);
                            ib_SubCategoryProductList.setVisibility(View.VISIBLE);
                            dl_SubCategoryList.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                        } else {
                            dl_SubCategoryList.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                            // atv_SubCategoryProductList.setVisibility(View.GONE);
                            ib_SubCategoryProductList.setVisibility(View.GONE);
                        }
                    } else {
                        dl_SubCategoryList.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                        // atv_SubCategoryProductList.setVisibility(View.GONE);
                        ib_SubCategoryProductList.setVisibility(View.GONE);
                    }
                }, error -> pb_ProductListLoader.setVisibility(View.GONE)) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", ConstantFields.getGuestToken());
                return params;
            }
        };

        stringRequest.setShouldCache(false);
        ApplicationContext.getInstance().addToRequestQueue(stringRequest, "SubCategoryList");

    }

    private void loadProductList() {

        if (menuHandler.networkError()) {
            pb_ProductListLoader.setVisibility(View.VISIBLE);

            StringRequest stringRequest = new StringRequest(Request.Method.GET, ConstantFields.getProductList(String.valueOf(mCategoryId + "," + modelCategoryDetail.getSubcategory()), page, mLoadType, filter, ConstantFields.currentLanguage().equals("en")),
                    response -> {
                        pb_ProductListLoader.setVisibility(View.GONE);

                        modelProductDetailLists = ConstantDataParser.getProductList(response);

                        if (mViewType == 1) {
                            productListAdapter = new ProductListAdapter(activity, modelProductDetailLists, true, menuHandler, rc_ProductList);
                        } else {
                            productListAdapter = new ProductListAdapter(activity, modelProductDetailLists, false, menuHandler, rc_ProductList);
                        }

                        rc_ProductList.setAdapter(productListAdapter);

                        productListAdapter.setOnLoadMoreListener(() -> {
                            page++;
                            if (modelProductDetailLists != null) {
                                if (modelProductDetailLists.size() > 0) {
                                    if (modelProductDetailLists.get(0) != null) {
                                        if (modelProductDetailLists.get(0).getTotal_count() > 0) {
                                            if (page <= ((modelProductDetailLists.get(0).getTotal_count() / 8) + 1)) {
                                                if (menuHandler.networkError()) {
                                                    pb_ProductListLoader.setVisibility(View.VISIBLE);
                                                    StringRequest stringRequest1 = new StringRequest(Request.Method.GET, ConstantFields.getProductList(String.valueOf(mCategoryId + "," + modelCategoryDetail.getSubcategory()), page, "Default", filter, ConstantFields.currentLanguage().equals("en")),
                                                            response1 -> {
                                                                pb_ProductListLoader.setVisibility(View.GONE);

                                                                ArrayList<ModelProductDetailList> tempList;
                                                                tempList = ConstantDataParser.getProductList(response1);
                                                                if (modelProductDetailLists.size() > 0) {
                                                                    if (tempList != null) {
                                                                        if (tempList.size() > 0) {
                                                                            modelProductDetailLists.addAll(tempList);
                                                                        }
                                                                    }
                                                                }
                                                                productListAdapter.notifyDataSetChanged();
                                                                productListAdapter.setLoaded();
                                                            }, error -> pb_ProductListLoader.setVisibility(View.GONE)) {

                                                        @Override
                                                        public Map<String, String> getHeaders() {
                                                            Map<String, String> params = new HashMap<>();
                                                            params.put("Content-Type", "application/json; charset=utf-8");
                                                            params.put("Authorization", ConstantFields.getGuestToken());
                                                            return params;
                                                        }
                                                    };

                                                    stringRequest1.setShouldCache(false);
                                                    ApplicationContext.getInstance().addToRequestQueue(stringRequest1, "ProductListPagination");
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                        });
                    }, error -> {
                pb_ProductListLoader.setVisibility(View.GONE);
                try {
                    if (error.networkResponse != null) {
                        String responseBody = new String(error.networkResponse.data, "utf-8");
                        JSONObject jsonObject = new JSONObject(responseBody);
                        Log.e("onErrorResponse: ", jsonObject.toString());
                    } else {
                        showToast(getString(R.string.user_error_invalid_user_detail));
                    }
                } catch (Exception e) {
                    showToast(getString(R.string.user_error_invalid_user_detail));
                }
            }) {

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<>();
                    params.put("Content-Type", "application/json; charset=utf-8");
                    params.put("Authorization", ConstantFields.getGuestToken());
                    return params;
                }
            };

            stringRequest.setShouldCache(false);
            ApplicationContext.getInstance().addToRequestQueue(stringRequest, "ProductList");
        }
    }

    private void loadSubCategoryProductList(final String categoryId) {
        if (menuHandler.networkError()) {
            pb_ProductListLoader.setVisibility(View.VISIBLE);

            StringRequest stringRequest = new StringRequest(Request.Method.GET, ConstantFields.getProductList(categoryId, page, mLoadType, filter, ConstantFields.currentLanguage().equals("en")),
                    response -> {
                        pb_ProductListLoader.setVisibility(View.GONE);

                        modelProductDetailLists = ConstantDataParser.getProductList(response);

                        if (mViewType == 1) {
                            productListAdapter = new ProductListAdapter(activity, modelProductDetailLists, true, menuHandler, rc_ProductList);
                        } else {
                            productListAdapter = new ProductListAdapter(activity, modelProductDetailLists, false, menuHandler, rc_ProductList);
                        }

                        rc_ProductList.setAdapter(productListAdapter);

                        productListAdapter.setOnLoadMoreListener(() -> {
                            page++;
                            if (modelProductDetailLists != null) {
                                if (modelProductDetailLists.size() > 0) {
                                    if (modelProductDetailLists.get(0) != null) {
                                        if (modelProductDetailLists.get(0).getTotal_count() > 0) {
                                            if (page <= ((modelProductDetailLists.get(0).getTotal_count() / 10) + 1)) {
                                                if (menuHandler.networkError()) {
                                                    pb_ProductListLoader.setVisibility(View.VISIBLE);
                                                    StringRequest stringRequest1 = new StringRequest(Request.Method.GET, ConstantFields.getProductList(categoryId, page, "Default", filter, ConstantFields.currentLanguage().equals("en")),
                                                            response1 -> {
                                                                pb_ProductListLoader.setVisibility(View.GONE);
                                                                ArrayList<ModelProductDetailList> tempList;
                                                                tempList = ConstantDataParser.getProductList(response1);
                                                                if (modelProductDetailLists.size() > 0) {
                                                                    if (tempList != null) {
                                                                        if (tempList.size() > 0) {
                                                                            modelProductDetailLists.addAll(tempList);
                                                                        }
                                                                    }
                                                                }
                                                                productListAdapter.notifyDataSetChanged();
                                                                productListAdapter.setLoaded();
                                                            }, error -> pb_ProductListLoader.setVisibility(View.GONE)) {

                                                        @Override
                                                        public Map<String, String> getHeaders() {
                                                            Map<String, String> params = new HashMap<>();
                                                            params.put("Content-Type", "application/json; charset=utf-8");
                                                            params.put("Authorization", ConstantFields.getGuestToken());
                                                            return params;
                                                        }
                                                    };

                                                    stringRequest1.setShouldCache(false);
                                                    ApplicationContext.getInstance().addToRequestQueue(stringRequest1, "ProductListPagination");
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                        });

                    }, error -> {
                pb_ProductListLoader.setVisibility(View.GONE);
                try {
                    if (error.networkResponse != null) {
                           /* String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject jsonObject = new JSONObject(responseBody);
                            Log.e("onErrorResponse: ", jsonObject.toString());*/
                        showToast(getString(R.string.user_error_invalid_user_detail));
                    } else {
                        showToast(getString(R.string.user_error_invalid_user_detail));
                    }
                } catch (Exception e) {
                    showToast(getString(R.string.user_error_invalid_user_detail));
                }
            }) {

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<>();
                    params.put("Content-Type", "application/json; charset=utf-8");
                    params.put("Authorization", ConstantFields.getGuestToken());
                    return params;
                }
            };

            stringRequest.setShouldCache(false);
            ApplicationContext.getInstance().addToRequestQueue(stringRequest, "ProductList");
        }
    }

    private void showToast(String message) {
        ConstantFields.CustomToast(message, activity);
    }

    @Override
    public void toProductList(int categoryId, int hasProduct) {
        mSubcategoryVisibleStatus = 1;
        dl_SubCategoryList.closeDrawers();
        menuHandler.toProductList(categoryId, "default", "");
    }

    @Override
    public void onResume() {
        super.onResume();

        if (dl_SubCategoryList != null) {
            mSubcategoryVisibleStatus = 1;
            dl_SubCategoryList.closeDrawers();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (dl_SubCategoryList != null) {
            mSubcategoryVisibleStatus = 1;
            dl_SubCategoryList.closeDrawers();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(AppLanguageSupport.onAttach(context));
        activity = (Activity) context;
        menuHandler = (MenuHandler) context;
        productListHelper = this;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (mLoadType.equals("default-deals")) {
            menuHandler.changeBottomView(4);
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.atv_name_az_sorting:
                mSortingList.dismiss();
                menuHandler.toProductList(mCategoryId, "A-Z", filter);
                break;
            case R.id.atv_name_za_sorting:
                mSortingList.dismiss();
                menuHandler.toProductList(mCategoryId, "Z-A", filter);
                break;
            case R.id.atv_price_lh_sorting:
                mSortingList.dismiss();
                menuHandler.toProductList(mCategoryId, "prL-prH", filter);
                break;
            case R.id.atv_price_hl_sorting:
                mSortingList.dismiss();
                menuHandler.toProductList(mCategoryId, "prH-prL", filter);
                break;
            case R.id.atv_position_az_sorting:
                mSortingList.dismiss();
                menuHandler.toProductList(mCategoryId, "poL-poH", filter);
                break;
            case R.id.atv_position_za_sorting:
                mSortingList.dismiss();
                menuHandler.toProductList(mCategoryId, "poH-poL", filter);
                break;
        }
    }

    private void fontSetup() {
        fontSetup(atv_ParentCategoryTitle, Constant.LIGHT);
    }

    private void fontSetup(Object o_SetFont, int type) {
        Constant.SetFontStyle(o_SetFont, type, Constant.C_TEXT_VIEW);
    }
}
