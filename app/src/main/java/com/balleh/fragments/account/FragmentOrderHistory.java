package com.balleh.fragments.account;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.balleh.R;
import com.balleh.additional.AppLanguageSupport;
import com.balleh.additional.ApplicationContext;
import com.balleh.additional.Constant;
import com.balleh.additional.ConstantDataParser;
import com.balleh.additional.ConstantDataSaver;
import com.balleh.additional.ConstantFields;
import com.balleh.additional.instentTransfer.MenuHandler;
import com.balleh.model.ModelAccountDetail;
import com.balleh.model.ModelOrderHistory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FragmentOrderHistory extends Fragment {

    private View v_OrderHistoryHolder;
    private Activity activity;
    private MenuHandler menuHandler;
    private RecyclerView rc_OrderList;
    private int page = 1;
    private ArrayList<ModelOrderHistory> orderList = new ArrayList<>();
    private OrderHistoryAdapter orderHistoryAdapter;
    private ProgressBar pb_Loader;
    private AppCompatTextView atv_OrderHistoryTitle;
    private Button btn_Continue;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v_OrderHistoryHolder = inflater.inflate(R.layout.fragment_order_history, container, false);
        load();
        return v_OrderHistoryHolder;
    }

    private void load() {
        atv_OrderHistoryTitle = v_OrderHistoryHolder.findViewById(R.id.atv_order_history_title);
        rc_OrderList = v_OrderHistoryHolder.findViewById(R.id.rc_order_history);
        btn_Continue = v_OrderHistoryHolder.findViewById(R.id.btn_continue_oh);
        pb_Loader = v_OrderHistoryHolder.findViewById(R.id.pb_loader_oh);

        rc_OrderList.setLayoutManager(new LinearLayoutManager(activity));
        btn_Continue.setOnClickListener(view -> menuHandler.backBtnPressed());

        pb_Loader.setVisibility(View.GONE);
        requestOrderList();
        fontSetup();
    }

    private void requestOrderList() {
        if (!ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mTokenTag).equals("Empty")) {
            if (!ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mCustomerEmailIdTag).equals("Empty")) {
                page = 1;
                loadOrderList(ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mCustomerEmailIdTag), page);
            } else {
                loadAccountInformation();
            }
        } else {
            menuHandler.toSignIn();
        }
    }

    private void loadAccountInformation() {
        if (menuHandler.networkError()) {
            if (!ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mTokenTag).equals("Empty")) {

                StringRequest stringRequest = new StringRequest(Request.Method.GET, ConstantFields.getCustomerDetail(ConstantFields.currentLanguage().equals("en")),
                        response -> {
                            Log.e("onResponse: ", response + "");
                            ModelAccountDetail modelAccountDetail = ConstantDataParser.getAccountDetail(response);

                            if (modelAccountDetail != null) {
                                if (modelAccountDetail.getEmailId() != null) {
                                    ConstantDataSaver.mStoreSharedPreferenceString(activity.getApplicationContext(),
                                            ConstantFields.mCustomerEmailIdTag, modelAccountDetail.getEmailId());

                                    page = 1;
                                    loadOrderList(modelAccountDetail.getEmailId(), page);
                                }
                            }

                        }, error -> {
                    if (error.networkResponse != null) {
                        if (error.networkResponse.statusCode == 401) {
                            ConstantDataSaver.mRemoveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mTokenTag);
                        }
                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> params = new HashMap<>();
                        params.put("Content-Type", "application/json");
                        params.put("Authorization", "Bearer "
                                + ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(),
                                ConstantFields.mTokenTag));
                        return params;
                    }
                };

                stringRequest.setShouldCache(false);
                ApplicationContext.getInstance().addToRequestQueue(stringRequest, "CustomerAccountInformation");
            }
        }
    }

    private void loadOrderList(String email, int page) {
        if (menuHandler.networkError()) {
            pb_Loader.setVisibility(View.VISIBLE);
            Log.e("loadOrderList: ", ConstantFields.getCustomerOrderListWithPagination(email, page, ConstantFields.currentLanguage().equals("en")) + "");
            StringRequest stringRequest = new StringRequest(Request.Method.GET, ConstantFields.getCustomerOrderListWithPagination(email, page, ConstantFields.currentLanguage().equals("en")),
                    response -> {
                        pb_Loader.setVisibility(View.GONE);
                        Log.e("onResponse: ", response + "");
                        orderList = ConstantDataParser.getOrderList(response);
                        setOrderList();
                    }, error -> {
                pb_Loader.setVisibility(View.GONE);
                if (error.networkResponse != null) {
                    if (error.networkResponse.statusCode == 401) {
                        ConstantDataSaver.mRemoveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mTokenTag);
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<>();
                    params.put("Content-Type", "application/json");
                    params.put("Authorization", ConstantFields.getGuestToken());
                    return params;
                }
            };

            stringRequest.setShouldCache(false);
            ApplicationContext.getInstance().addToRequestQueue(stringRequest, "CustomerOrderInformation");
        }
    }

    private void setOrderList() {

        orderHistoryAdapter = new OrderHistoryAdapter();

        orderHistoryAdapter.setOnLoadMoreListener(() -> {
            page++;
            if (orderList != null) {
                if (orderList.size() > 0) {
                    if (orderList.get(0) != null) {
                        if (orderList.get(0).getOrderListCount() > 0) {
                            if (page <= ((orderList.get(0).getOrderListCount() / 8) + 1)) {
                                if (menuHandler.networkError()) {
                                    if (!ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mCustomerEmailIdTag).equals("Empty")) {
                                        pb_Loader.setVisibility(View.VISIBLE);
                                        StringRequest stringRequest = new StringRequest(Request.Method.GET, ConstantFields.getCustomerOrderListWithPagination(ConstantDataSaver
                                                .mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mCustomerEmailIdTag), page, ConstantFields.currentLanguage().equals("en")),
                                                response -> {
                                                    pb_Loader.setVisibility(View.GONE);
                                                    Log.e("onResponse: ", response + "");

                                                    ArrayList<ModelOrderHistory> tempList = ConstantDataParser.getOrderList(response);

                                                    if (tempList != null) {
                                                        if (tempList.size() > 0) {
                                                            orderList.addAll(tempList);
                                                        }
                                                    }

                                                    orderHistoryAdapter.notifyDataSetChanged();
                                                    orderHistoryAdapter.setLoaded();

                                                }, error -> {
                                            pb_Loader.setVisibility(View.GONE);
                                            if (error.networkResponse != null) {
                                                if (error.networkResponse.statusCode == 401) {
                                                    ConstantDataSaver.mRemoveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mTokenTag);
                                                }
                                            }
                                        }) {
                                            @Override
                                            public Map<String, String> getHeaders() {
                                                Map<String, String> params = new HashMap<>();
                                                params.put("Content-Type", "application/json");
                                                params.put("Authorization", ConstantFields.getGuestToken());
                                                return params;
                                            }
                                        };

                                        stringRequest.setShouldCache(false);
                                        ApplicationContext.getInstance().addToRequestQueue(stringRequest, "CustomerOrderInformationPagination");
                                    }
                                }
                            }
                        }
                    }
                }
            }
        });

        rc_OrderList.setAdapter(orderHistoryAdapter);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(AppLanguageSupport.onAttach(context));
        menuHandler = (MenuHandler) context;
        activity = (Activity) context;
    }

    class OrderHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private OnLoadMoreListener onLoadMoreListener;
        private int visibleThreshold = 10;
        private int lastVisibleItem, totalItemCount;
        private boolean loading;

        OrderHistoryAdapter() {
            if (rc_OrderList.getLayoutManager() instanceof LinearLayoutManager) {

                final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) rc_OrderList.getLayoutManager();
                rc_OrderList.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                        super.onScrolled(recyclerView, dx, dy);

                        totalItemCount = linearLayoutManager.getItemCount();
                        lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                        if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {

                            if (onLoadMoreListener != null) {
                                onLoadMoreListener.onLoadMore();
                            }
                            loading = true;
                        }
                    }
                });
            }
        }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

            RecyclerView.ViewHolder viewHolder;

            if (viewType == 1) {
                viewHolder = new OrderHistoryViewHolder(LayoutInflater.from(activity).inflate(R.layout.rc_row_order_history, parent, false));
            } else {
                viewHolder = new EmptyOrderList(LayoutInflater.from(activity).inflate(R.layout.rc_row_empty, parent, false));
            }

            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            if (holder.getItemViewType() == 1) {
                OrderHistoryViewHolder orderHistoryViewHolder = (OrderHistoryViewHolder) holder;
                orderHistoryViewHolder.atv_OrderId.setText(orderList.get(position).getOrderId());

                String orderStatus = orderList.get(position).getOrderStatus();

                if (orderStatus != null) {
                    if (orderStatus.toLowerCase().contains("pending")) {
                        orderHistoryViewHolder.atv_OrderStatusValue.setTextColor(ContextCompat.getColor(activity, R.color.order_pending_color));
                    } else if (orderStatus.toLowerCase().contains("complete")) {
                        orderHistoryViewHolder.atv_OrderStatusValue.setTextColor(ContextCompat.getColor(activity, R.color.order_success_color));
                    } else if (orderStatus.toLowerCase().contains("cancel")) {
                        orderHistoryViewHolder.atv_OrderStatusValue.setTextColor(ContextCompat.getColor(activity, R.color.order_cancel_color));
                    } else {
                        orderHistoryViewHolder.atv_OrderStatusValue.setTextColor(ContextCompat.getColor(activity, R.color.order_unknown_color));
                    }
                }

                orderHistoryViewHolder.atv_OrderStatusValue.setText(orderStatus);

                String orderDate = getString(R.string.single_order_date) + " " + orderList.get(position).getOrderDate();
                orderHistoryViewHolder.atv_OrderDate.setText(orderDate);

                String name = orderList.get(position).getOrderCustomerFirstName() + " " + orderList.get(position).getOrderCustomerLastName();
                orderHistoryViewHolder.atv_OrderCustomerName.setText(name);

                String orderTotal = getString(R.string.order_history_total_title) + " " + orderList.get(position).getOrderTotal();
                orderHistoryViewHolder.atv_OrderTotal.setText(orderTotal);

            } else {
                EmptyOrderList emptyOrderList = (EmptyOrderList) holder;
                emptyOrderList.atv_EmptyMessage.setText(getString(R.string.order_history_empty_list));
            }
        }

        @Override
        public int getItemCount() {
            if (orderList != null) {
                if (orderList.size() > 0) {
                    return orderList.size();
                } else {
                    return 1;
                }
            } else {
                return 1;
            }
        }

        @Override
        public int getItemViewType(int position) {
            if (orderList != null) {
                if (orderList.size() > 0) {
                    return 1;
                } else {
                    return 2;
                }
            } else {
                return 2;
            }
        }

        private class OrderHistoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            AppCompatTextView atv_OrderId, atv_OrderStatusValue, atv_OrderCustomerName,
                    atv_OrderDate, atv_OrderTotal;
            TextView tv_ToOrderDetail;

            OrderHistoryViewHolder(View view) {
                super(view);
                atv_OrderId = view.findViewById(R.id.atv_order_id_oh);
                atv_OrderStatusValue = view.findViewById(R.id.atv_order_status_value_oh);
                atv_OrderCustomerName = view.findViewById(R.id.atv_order_customer_name_oh);
                atv_OrderDate = view.findViewById(R.id.atv_order_date_oh);
                atv_OrderTotal = view.findViewById(R.id.atv_order_total_oh);
                tv_ToOrderDetail = view.findViewById(R.id.tv_to_order_details);

                tv_ToOrderDetail.setOnClickListener(this);

                fontSetup(atv_OrderId, Constant.LIGHT, Constant.C_TEXT_VIEW);
                fontSetup(atv_OrderStatusValue, Constant.LIGHT, Constant.C_TEXT_VIEW);
                fontSetup(atv_OrderCustomerName, Constant.LIGHT, Constant.C_TEXT_VIEW);
                fontSetup(atv_OrderDate, Constant.LIGHT, Constant.C_TEXT_VIEW);
                fontSetup(atv_OrderTotal, Constant.LIGHT, Constant.C_TEXT_VIEW);
                fontSetup(tv_ToOrderDetail, Constant.LIGHT, Constant.C_TEXT_VIEW);
            }

            @Override
            public void onClick(View view) {
                Log.e( "onClick: ",getAdapterPosition()+"" );
                if (view.getId() == R.id.tv_to_order_details) {
                    if (getAdapterPosition() != -1)
                        menuHandler.loadSingleOrderHistory(orderList.get(getAdapterPosition()).getOrderDetail());
                }
            }
        }

        private class EmptyOrderList extends RecyclerView.ViewHolder {
            AppCompatTextView atv_EmptyMessage;

            EmptyOrderList(View view) {
                super(view);
                atv_EmptyMessage = view.findViewById(R.id.atv_empty_message);
                fontSetup(atv_EmptyMessage, Constant.LIGHT, Constant.C_TEXT_VIEW);
            }
        }

        void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
            this.onLoadMoreListener = onLoadMoreListener;
        }

        public void setLoaded() {
            loading = false;
        }
    }

    public interface OnLoadMoreListener {
        void onLoadMore();
    }


    private void fontSetup() {
        fontSetup(atv_OrderHistoryTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);

        fontSetup(btn_Continue, Constant.LIGHT, Constant.C_BUTTON);
    }

    private void fontSetup(Object o_SetFont, int type, int which) {
        Constant.SetFontStyle(o_SetFont, type, which);
    }
}
