package com.balleh.fragments.account;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.balleh.R;
import com.balleh.additional.AppLanguageSupport;
import com.balleh.additional.ApplicationContext;
import com.balleh.additional.Constant;
import com.balleh.additional.ConstantDataParser;
import com.balleh.additional.ConstantDataSaver;
import com.balleh.additional.ConstantFields;
import com.balleh.additional.instentTransfer.MenuHandler;
import com.balleh.model.ModelAccountDetail;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class FragmentEditAccount extends Fragment {

    private MenuHandler menuHandler;
    private View v_AccountHolder;
    private String mGender = null, mDOBDate = null;
    private DatePickerDialog datePickerDialog;
    private Activity activity;
    private ProgressBar pb_AccountEditLoader;
    private Button btn_Continue, btn_Back;
    private EditText et_FirstName, et_LastName, et_MobileNumber;
    private AppCompatTextView atv_EditAccountDOB, atv_EditAccountEmail, atv_EditAccountFirstNameError,
            atv_EditAccountLastNameError, atv_EditAccountMobileNumberError, atv_EditAccountTitle1, atv_EditAccountTitle2,
            atv_EditAccountFirstNameTitle, atv_EditAccountLastNameTitle, atv_EditAccountEMailTitle, atv_EditAccountGenderTitle,
            atv_EditAccountMobileNumberTitle, atv_EditAccountDOBTitle;
    private Spinner s_GenderList;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v_AccountHolder = inflater.inflate(R.layout.fragment_account_edit, container, false);
        load();
        loadAccountInformation();
        return v_AccountHolder;
    }

    private void load() {
        s_GenderList = v_AccountHolder.findViewById(R.id.s_gender_list_edit_account);

        et_FirstName = v_AccountHolder.findViewById(R.id.et_first_name_edit_account);
        et_LastName = v_AccountHolder.findViewById(R.id.et_last_name_edit_account);
        et_MobileNumber = v_AccountHolder.findViewById(R.id.et_mobile_no_edit_account);

        atv_EditAccountFirstNameError = v_AccountHolder.findViewById(R.id.atv_first_name_error_edit_account);
        atv_EditAccountLastNameError = v_AccountHolder.findViewById(R.id.atv_last_name_error_edit_account);
        atv_EditAccountDOB = v_AccountHolder.findViewById(R.id.atv_dob_edit_account);
        atv_EditAccountMobileNumberError = v_AccountHolder.findViewById(R.id.atv_error_mobile_edit_account);
        atv_EditAccountEmail = v_AccountHolder.findViewById(R.id.atv_email_edit_account);
        atv_EditAccountTitle1 = v_AccountHolder.findViewById(R.id.atv_account_title_1);
        atv_EditAccountTitle2 = v_AccountHolder.findViewById(R.id.atv_account_title_2);
        atv_EditAccountFirstNameTitle = v_AccountHolder.findViewById(R.id.atv_first_name_title_account);
        atv_EditAccountLastNameTitle = v_AccountHolder.findViewById(R.id.atv_last_name_account);
        atv_EditAccountEMailTitle = v_AccountHolder.findViewById(R.id.atv_email_title_account);
        atv_EditAccountGenderTitle = v_AccountHolder.findViewById(R.id.atv_dob_title_account);
        atv_EditAccountMobileNumberTitle = v_AccountHolder.findViewById(R.id.atv_gender_title_account);
        atv_EditAccountDOBTitle = v_AccountHolder.findViewById(R.id.atv_mobile_title_account);

        btn_Continue = v_AccountHolder.findViewById(R.id.btn_continue_edit_account);
        btn_Back = v_AccountHolder.findViewById(R.id.btn_back_edit_account);

        pb_AccountEditLoader = v_AccountHolder.findViewById(R.id.pb_account_edit);
        fontSetup();
    }

    private void load(final ModelAccountDetail modelAccountDetail) {

        final ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(activity,
                R.array.user_reg_gender_list, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        s_GenderList.setAdapter(adapter);

        et_FirstName.setText(modelAccountDetail.getFirstName());
        et_LastName.setText(modelAccountDetail.getLastName());

        if (modelAccountDetail.getDob() != null)
            if (modelAccountDetail.getDob().length() > 0)
                atv_EditAccountDOB.setText(modelAccountDetail.getDob());

        if (modelAccountDetail.getMobileNumber() != null)
            et_MobileNumber.setText(modelAccountDetail.getMobileNumber());
        atv_EditAccountEmail.setText(modelAccountDetail.getEmailId());

        s_GenderList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (adapterView.getSelectedItem().toString() != null) {
                    if (adapterView.getSelectedItem().toString().length() > 0) {
                        mGender = adapterView.getSelectedItem().toString().trim();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        int mYear, mMonth, mDay;
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        datePickerDialog = new DatePickerDialog(activity, (datePicker, Year, Month, Day) -> {
            mDOBDate = Day + "-" + (Month + 1) + "-" + Year;
            atv_EditAccountDOB.setText(mDOBDate);
        }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());


        atv_EditAccountDOB.setOnClickListener(view -> datePickerDialog.show());

        btn_Back.setOnClickListener(view -> menuHandler.backBtnPressed());

        btn_Continue.setOnClickListener(view -> {
            if (!et_FirstName.getText().toString().equals("")
                    && (et_FirstName.getText().toString().length() > 3)

                    && !et_LastName.getText().toString().equals("")
                    && (et_LastName.getText().toString().length() > 3)

                    && !et_MobileNumber.getText().toString().equals("")
                    && et_MobileNumber.getText().toString().length() == 10) {

                try {
                    JSONObject jsonObjectCustomer = new JSONObject();
                    jsonObjectCustomer.put("attribute_code", "reg_mobile_no");
                    jsonObjectCustomer.put("value", et_MobileNumber.getEditableText().toString());

                    JSONArray jsonArray = new JSONArray();
                    jsonArray.put(jsonObjectCustomer);

                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("firstname", et_FirstName.getEditableText().toString());
                    jsonObject.put("lastname", et_LastName.getEditableText().toString());
                    jsonObject.put("email", atv_EditAccountEmail.getText().toString());
                    jsonObject.put("websiteId", 1);

                    jsonObject.put("custom_attributes", jsonArray);

                    if (mGender != null) {
                        if (mGender.length() > 0) {
                            switch (mGender) {
                                case "Male":
                                    jsonObject.put("gender", "1");
                                    break;
                                case "Female":
                                    jsonObject.put("gender", "2");
                                    break;
                                case "Not Specified":
                                    jsonObject.put("gender", "3");
                                    break;
                            }
                        }
                    }

                    if (atv_EditAccountDOB.getText().toString().length() > 0) {
                        jsonObject.put("dob", atv_EditAccountDOB.getText().toString());
                    }

                    final JSONObject mAccountDetail = new JSONObject();
                    mAccountDetail.put("customer", jsonObject);

                    Log.e("onClick: ", mAccountDetail.toString());


                    if (menuHandler.networkError()) {
                        pb_AccountEditLoader.setVisibility(View.VISIBLE);

                        StringRequest stringRequest = new StringRequest(Request.Method.PUT, ConstantFields.getCustomerDetail(ConstantFields.currentLanguage().equals("en")),
                                response -> {
                                    pb_AccountEditLoader.setVisibility(View.GONE);
                                    Log.e("onResponse: ", response + "");
                                    ConstantFields.CustomToast(getString(R.string.edit_account_success), activity);
                                    menuHandler.backBtnPressed();
                                }, error -> {
                            pb_AccountEditLoader.setVisibility(View.GONE);
                            ConstantFields.CustomToast(getString(R.string.common_error), getActivity());
                            menuHandler.backBtnPressed();
                            if (error.networkResponse != null) {
                                if (error.networkResponse.statusCode == 401) {
                                    ConstantDataSaver.mRemoveSharedPreferenceString(activity, ConstantFields.mTokenTag);
                                }
                            }
                        }) {

                            @Override
                            public byte[] getBody() {
                                return mAccountDetail.toString().getBytes();
                            }

                            @Override
                            public Map<String, String> getHeaders() {
                                Map<String, String> params = new HashMap<>();
                                params.put("Content-Type", "application/json");
                                params.put("Authorization", "Bearer "
                                        + ConstantDataSaver.mRetrieveSharedPreferenceString(activity, ConstantFields.mTokenTag));
                                return params;
                            }
                        };

                        stringRequest.setShouldCache(false);
                        ApplicationContext.getInstance().addToRequestQueue(stringRequest, "CustomerAccountInformation");
                    }

                } catch (Exception e) {
                    Log.e("onClick: ", e.toString());
                    ConstantFields.CustomToast(getString(R.string.common_error), getActivity());
                }
            }

            if (et_FirstName.getText().toString().equals("")) {
                atv_EditAccountFirstNameError.setVisibility(View.VISIBLE);
            } else {
                atv_EditAccountFirstNameError.setVisibility(View.GONE);
            }
            if (et_LastName.getText().toString().equals("")) {
                atv_EditAccountLastNameError.setVisibility(View.VISIBLE);
            } else {
                atv_EditAccountLastNameError.setVisibility(View.GONE);
            }
            if (et_MobileNumber.getText().toString().equals("")) {
                atv_EditAccountMobileNumberError.setVisibility(View.VISIBLE);
            } else {
                if (et_MobileNumber.getText().toString().length() == 10) {
                    atv_EditAccountMobileNumberError.setVisibility(View.GONE);
                } else {
                    atv_EditAccountMobileNumberError.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void loadAccountInformation() {
        if (menuHandler.networkError()) {
            if (!ConstantDataSaver.mRetrieveSharedPreferenceString(activity, ConstantFields.mTokenTag).equals("Empty")) {
                pb_AccountEditLoader.setVisibility(View.VISIBLE);
                StringRequest stringRequest = new StringRequest(Request.Method.GET, ConstantFields.getCustomerDetail(ConstantFields.currentLanguage().equals("en")),
                        response -> {
                            pb_AccountEditLoader.setVisibility(View.GONE);
                            Log.e("onResponse: ", response + "");
                            ModelAccountDetail modelAccountDetail = ConstantDataParser.getAccountDetail(response);
                            if (modelAccountDetail != null)
                                load(modelAccountDetail);
                        }, error -> {
                    pb_AccountEditLoader.setVisibility(View.GONE);
                    if (error.networkResponse != null) {
                        if (error.networkResponse.statusCode == 401) {
                            ConstantDataSaver.mRemoveSharedPreferenceString(activity, ConstantFields.mTokenTag);
                        }
                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> params = new HashMap<>();
                        params.put("Content-Type", "application/json");
                        params.put("Authorization", "Bearer "
                                + ConstantDataSaver.mRetrieveSharedPreferenceString(activity, ConstantFields.mTokenTag));
                        return params;
                    }
                };

                stringRequest.setShouldCache(false);
                ApplicationContext.getInstance().addToRequestQueue(stringRequest, "CustomerAccountInformation");
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(AppLanguageSupport.onAttach(context));
        activity = (Activity) context;
        menuHandler = (MenuHandler) context;
    }

    private void fontSetup() {
        fontSetup(et_FirstName, Constant.LIGHT, Constant.C_EDIT_TEXT);
        fontSetup(et_LastName, Constant.LIGHT, Constant.C_EDIT_TEXT);
        fontSetup(et_MobileNumber, Constant.LIGHT, Constant.C_EDIT_TEXT);

        fontSetup(atv_EditAccountDOB, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_EditAccountEmail, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_EditAccountFirstNameError, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_EditAccountLastNameError, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_EditAccountMobileNumberError, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_EditAccountTitle1, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_EditAccountTitle2, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_EditAccountFirstNameTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_EditAccountLastNameTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_EditAccountEMailTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_EditAccountGenderTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_EditAccountMobileNumberTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_EditAccountDOBTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);

        fontSetup(btn_Continue, Constant.LIGHT, Constant.C_BUTTON);
        fontSetup(btn_Back, Constant.LIGHT, Constant.C_BUTTON);
    }

    private void fontSetup(Object o_SetFont, int type, int which) {
        Constant.SetFontStyle(o_SetFont, type, which);
    }
}
