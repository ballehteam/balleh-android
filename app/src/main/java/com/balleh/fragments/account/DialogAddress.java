package com.balleh.fragments.account;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.balleh.R;
import com.balleh.adapter.SpinnerCountryAdapter;
import com.balleh.adapter.SpinnerStateAdapter;
import com.balleh.additional.AppLanguageSupport;
import com.balleh.additional.ApplicationContext;
import com.balleh.additional.Constant;
import com.balleh.additional.ConstantDataParser;
import com.balleh.additional.ConstantDataSaver;
import com.balleh.additional.ConstantFields;
import com.balleh.additional.instentTransfer.MenuHandler;
import com.balleh.model.ModelAccountDetail;
import com.balleh.model.ModelAddress;
import com.balleh.model.ModelCountry;
import com.balleh.model.ModelState;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DialogAddress extends DialogFragment {

    private Activity activity;
    private View v_InsertAddress;
    private ModelAccountDetail accountDetail;
    private String addressType;
    private EditText et_FirstName, et_LastName, et_MobileNumber, et_StreetAddress,/* et_City,*/
            et_ZipCode;
    private AppCompatTextView atv_FirstNameError, atv_LastNameError, atv_MobileNumberError,
            atv_StreetAddressError, atv_CityError, atv_StateError, atv_CountryError, atv_ZipCodeError,
            atv_AddressTitle, atv_ContactTitle, atv_FirstNameTitle, atv_LastNameTitle, atv_MobileNoTitle, atv_AddressInfoTitle,
            atv_StreetAddressTitle, atv_CityTitle, atv_StateTitle, atv_CountryTitle, atv_ZipCodeTitle;
    private Button btn_SaveAddress;
    private CheckBox cb_BillingAddress, cb_ShippingAddress;
    private Spinner s_CityList, s_StateList, s_CountryList;
    private ArrayList<ModelCountry> CountryList;
    private String CountryId, StateCode, CityName = "Empty";
    private int StateId, AddressId = 0;
    private ProgressBar pb_AddressLoader;
    private MenuHandler menuHandler;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.getString("CustomerDetail") != null) {
                accountDetail = ConstantDataParser.getAccountDetail(bundle.getString("CustomerDetail"));
            } else {
                accountDetail = null;
            }

            if (bundle.getString("AddressType") != null) {
                addressType = bundle.getString("AddressType");

                if (addressType != null) {
                    if (addressType.length() > 0) {
                        if (bundle.getInt("AddressId") != 0) {
                            AddressId = bundle.getInt("AddressId");
                        }
                    }
                }

            }

        } else {
            accountDetail = null;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v_InsertAddress = inflater.inflate(R.layout.dialog_address, container, false);
        load();
        return v_InsertAddress;
    }

    private void load() {
        et_FirstName = v_InsertAddress.findViewById(R.id.et_first_name_address);
        et_LastName = v_InsertAddress.findViewById(R.id.et_last_name_address);
        et_MobileNumber = v_InsertAddress.findViewById(R.id.et_mobile_no_address);
        et_StreetAddress = v_InsertAddress.findViewById(R.id.et_street_address);
        //et_City = v_InsertAddress.findViewById(R.id.et_city_address);
        et_ZipCode = v_InsertAddress.findViewById(R.id.et_zip_code_address);

        atv_ContactTitle = v_InsertAddress.findViewById(R.id.atv_title_c_us);
        atv_FirstNameTitle = v_InsertAddress.findViewById(R.id.atv_first_name_title_c_us);
        atv_LastNameTitle = v_InsertAddress.findViewById(R.id.atv_last_name_title_c_us);
        atv_MobileNoTitle = v_InsertAddress.findViewById(R.id.atv_mobile_no_title_c_us);
        atv_AddressInfoTitle = v_InsertAddress.findViewById(R.id.atv_address_title_c_us);
        atv_StreetAddressTitle = v_InsertAddress.findViewById(R.id.atv_street_address_title_c_us);
        atv_CityTitle = v_InsertAddress.findViewById(R.id.atv_city_title_c_us);
        atv_StateTitle = v_InsertAddress.findViewById(R.id.atv_state_title_c_us);
        atv_CountryTitle = v_InsertAddress.findViewById(R.id.atv_country_title_c_us);
        atv_ZipCodeTitle = v_InsertAddress.findViewById(R.id.atv_zip_code_title_c_us);

        atv_AddressTitle = v_InsertAddress.findViewById(R.id.atv_address_title);
        atv_FirstNameError = v_InsertAddress.findViewById(R.id.atv_first_name_error_address);
        atv_LastNameError = v_InsertAddress.findViewById(R.id.atv_last_name_error_address);
        atv_MobileNumberError = v_InsertAddress.findViewById(R.id.atv_mobile_error_address);
        atv_StreetAddressError = v_InsertAddress.findViewById(R.id.atv_street_error_address);
        atv_CityError = v_InsertAddress.findViewById(R.id.atv_city_error_address);
        atv_StateError = v_InsertAddress.findViewById(R.id.atv_state_error_address);
        atv_CountryError = v_InsertAddress.findViewById(R.id.atv_country_error_address);
        atv_ZipCodeError = v_InsertAddress.findViewById(R.id.atv_zip_code_error_address);

        s_CityList = v_InsertAddress.findViewById(R.id.s_city_address);
        s_StateList = v_InsertAddress.findViewById(R.id.s_state_address);
        s_CountryList = v_InsertAddress.findViewById(R.id.s_country_address);

        cb_BillingAddress = v_InsertAddress.findViewById(R.id.cb_as_billing_address);
        cb_ShippingAddress = v_InsertAddress.findViewById(R.id.cb_as_shipping_address);
        btn_SaveAddress = v_InsertAddress.findViewById(R.id.btn_save_address);

        pb_AddressLoader = v_InsertAddress.findViewById(R.id.pb_address_loader);

        if (addressType != null) {
            if (addressType.length() > 0) {
                switch (addressType) {
                    case "New":
                        atv_AddressTitle.setText(getString(R.string.address_new_title));
                        break;
                    case "edit":
                        atv_AddressTitle.setText(getString(R.string.address_edit_title));

                        ModelAddress addressDetailEdit;

                        if (AddressId != 0) {
                            if (accountDetail != null) {
                                if (accountDetail.getAddressList() != null) {
                                    if (accountDetail.getAddressList().size() > 0) {
                                        for (int i = 0; i < accountDetail.getAddressList().size(); i++) {
                                            if (AddressId == accountDetail.getAddressList().get(i).getAddressId()) {

                                                addressDetailEdit = accountDetail.getAddressList().get(i);

                                                if (addressDetailEdit != null) {
                                                    if (addressDetailEdit.getFirstName() != null) {
                                                        if (addressDetailEdit.getFirstName().length() > 0) {
                                                            et_FirstName.setText(addressDetailEdit.getFirstName());
                                                        }
                                                    }
                                                    if (addressDetailEdit.getLastName() != null) {
                                                        if (addressDetailEdit.getLastName().length() > 0) {
                                                            et_LastName.setText(addressDetailEdit.getLastName());
                                                        }
                                                    }
                                                    if (addressDetailEdit.getTelephone() != null) {
                                                        if (addressDetailEdit.getTelephone().length() > 0) {
                                                            et_MobileNumber.setText(addressDetailEdit.getTelephone());
                                                        }
                                                    }
                                                   /* if (addressDetailEdit.getCity() != null) {
                                                        if (addressDetailEdit.getCity().length() > 0) {
                                                            et_City.setText(addressDetailEdit.getCity());
                                                        }
                                                    }*/
                                                    if (addressDetailEdit.getStreet() != null) {
                                                        if (addressDetailEdit.getStreet().length() > 0) {
                                                            et_StreetAddress.setText(addressDetailEdit.getStreet());
                                                        }
                                                    }
                                                    if (addressDetailEdit.getPost_code() != null) {
                                                        if (addressDetailEdit.getPost_code().length() > 0) {
                                                            et_ZipCode.setText(addressDetailEdit.getPost_code());
                                                        }
                                                    }
                                                }
                                            }

                                        }
                                    }
                                }
                            }
                        }
                        break;
                    case "Shipping":
                        addressType = "edit";
                        cb_ShippingAddress.setChecked(true);
                        cb_ShippingAddress.setEnabled(false);
                        cb_ShippingAddress.setText(R.string.address_shipping_address);

                        ModelAddress addressDetailShipping;

                        if (AddressId != 0) {
                            if (accountDetail != null) {
                                if (accountDetail.getAddressList() != null) {
                                    if (accountDetail.getAddressList().size() > 0) {
                                        for (int i = 0; i < accountDetail.getAddressList().size(); i++) {
                                            if (AddressId == accountDetail.getAddressList().get(i).getAddressId()) {

                                                addressDetailShipping = accountDetail.getAddressList().get(i);

                                                if (addressDetailShipping != null) {
                                                    if (addressDetailShipping.getFirstName() != null) {
                                                        if (addressDetailShipping.getFirstName().length() > 0) {
                                                            et_FirstName.setText(addressDetailShipping.getFirstName());
                                                        }
                                                    }
                                                    if (addressDetailShipping.getLastName() != null) {
                                                        if (addressDetailShipping.getLastName().length() > 0) {
                                                            et_LastName.setText(addressDetailShipping.getLastName());
                                                        }
                                                    }
                                                    if (addressDetailShipping.getTelephone() != null) {
                                                        if (addressDetailShipping.getTelephone().length() > 0) {
                                                            et_MobileNumber.setText(addressDetailShipping.getTelephone());
                                                        }
                                                    }
                                                   /* if (addressDetailShipping.getCity() != null) {
                                                        if (addressDetailShipping.getCity().length() > 0) {
                                                            et_City.setText(addressDetailShipping.getCity());
                                                        }
                                                    }*/
                                                    if (addressDetailShipping.getStreet() != null) {
                                                        if (addressDetailShipping.getStreet().length() > 0) {
                                                            et_StreetAddress.setText(addressDetailShipping.getStreet());
                                                        }
                                                    }
                                                    if (addressDetailShipping.getPost_code() != null) {
                                                        if (addressDetailShipping.getPost_code().length() > 0) {
                                                            et_ZipCode.setText(addressDetailShipping.getPost_code());
                                                        }
                                                    }
                                                }
                                            }

                                        }
                                    }
                                }
                            }
                        }
                        break;
                    case "NewShipping":
                        cb_ShippingAddress.setChecked(true);
                        cb_ShippingAddress.setEnabled(false);
                        cb_ShippingAddress.setText(R.string.address_shipping_address);
                        break;
                    case "Billing":
                        addressType = "edit";
                        cb_BillingAddress.setChecked(true);
                        cb_BillingAddress.setEnabled(false);
                        cb_BillingAddress.setText(R.string.address_billing_address);

                        ModelAddress addressDetailBilling;

                        if (AddressId != 0) {
                            if (accountDetail != null) {
                                if (accountDetail.getAddressList() != null) {
                                    if (accountDetail.getAddressList().size() > 0) {
                                        for (int i = 0; i < accountDetail.getAddressList().size(); i++) {
                                            if (AddressId == accountDetail.getAddressList().get(i).getAddressId()) {

                                                addressDetailBilling = accountDetail.getAddressList().get(i);

                                                if (addressDetailBilling != null) {
                                                    if (addressDetailBilling.getFirstName() != null) {
                                                        if (addressDetailBilling.getFirstName().length() > 0) {
                                                            et_FirstName.setText(addressDetailBilling.getFirstName());
                                                        }
                                                    }
                                                    if (addressDetailBilling.getLastName() != null) {
                                                        if (addressDetailBilling.getLastName().length() > 0) {
                                                            et_LastName.setText(addressDetailBilling.getLastName());
                                                        }
                                                    }
                                                    if (addressDetailBilling.getTelephone() != null) {
                                                        if (addressDetailBilling.getTelephone().length() > 0) {
                                                            et_MobileNumber.setText(addressDetailBilling.getTelephone());
                                                        }
                                                    }
                                                 /*   if (addressDetailBilling.getCity() != null) {
                                                        if (addressDetailBilling.getCity().length() > 0) {
                                                            et_City.setText(addressDetailBilling.getCity());
                                                        }
                                                    }*/
                                                    if (addressDetailBilling.getStreet() != null) {
                                                        if (addressDetailBilling.getStreet().length() > 0) {
                                                            et_StreetAddress.setText(addressDetailBilling.getStreet());
                                                        }
                                                    }
                                                    if (addressDetailBilling.getPost_code() != null) {
                                                        if (addressDetailBilling.getPost_code().length() > 0) {
                                                            et_ZipCode.setText(addressDetailBilling.getPost_code());
                                                        }
                                                    }
                                                }
                                            }

                                        }
                                    }
                                }
                            }
                        }
                        break;
                    case "NewBilling":
                        cb_BillingAddress.setChecked(true);
                        cb_BillingAddress.setEnabled(false);
                        cb_BillingAddress.setText(R.string.address_billing_address);
                        break;
                    case "Both":
                        addressType = "edit";
                        cb_ShippingAddress.setChecked(true);
                        cb_ShippingAddress.setEnabled(false);
                        cb_BillingAddress.setChecked(true);
                        cb_BillingAddress.setEnabled(false);
                        cb_ShippingAddress.setText(R.string.address_shipping_address);
                        cb_BillingAddress.setText(R.string.address_billing_address);
                        break;
                    default:
                        atv_AddressTitle.setText(getString(R.string.address_safe_title));
                        break;
                }
            } else {
                atv_AddressTitle.setText(getString(R.string.address_safe_title));
            }
        } else {
            atv_AddressTitle.setText(getString(R.string.address_safe_title));
        }

        getCountry();

        fontSetup();
    }

    private void getCountry() {
        if (menuHandler.networkError()) {
            pb_AddressLoader.setVisibility(View.VISIBLE);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, ConstantFields.mCountry,
                    response -> {
                        Log.e("onResponse: ", response);
                        pb_AddressLoader.setVisibility(View.GONE);

                        setupBtn();

                        CountryList = ConstantDataParser.getCountryList(response);

                        if (CountryList != null) {
                            if (CountryList.size() > 0) {
                                SpinnerCountryAdapter countryAdapter = new SpinnerCountryAdapter(activity, CountryList);
                                s_CountryList.setAdapter(countryAdapter);

                                for (int i = 0; i < CountryList.size(); i++) {
                                    if (CountryList.get(i).getId().toLowerCase().equals("sa")) {
                                        s_CountryList.setSelection(i);
                                    }
                                }

                                s_CountryList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> adapterView, View view, int positionParent, long l) {

                                        if (CountryList.get(positionParent).getId() != null) {
                                            if (!CountryList.get(positionParent).getId().equals("-1")) {

                                                CountryId = CountryList.get(positionParent).getId();

                                                if (CountryList.get(positionParent).getStateList() != null) {

                                                    if (CountryList.get(positionParent).getStateList().size() > 0) {

                                                        final ArrayList<ModelState> stateSpinnerList = CountryList.get(positionParent).getStateList();
                                                        SpinnerStateAdapter adapterState = new SpinnerStateAdapter(activity, stateSpinnerList);
                                                        adapterState.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                                        s_StateList.getBaseline();
                                                        s_StateList.setAdapter(adapterState);

                                                        s_StateList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                                                            public void onItemSelected(AdapterView<?> parent, View view, int positionChild, long id) {
                                                                if (stateSpinnerList.get(positionChild).getId() != null) {
                                                                    StateCode = stateSpinnerList.get(positionChild).getCode();
                                                                    StateId = Integer.valueOf(stateSpinnerList.get(positionChild).getId());
                                                                    if (menuHandler.networkError()) {
                                                                        pb_AddressLoader.setVisibility(View.VISIBLE);
                                                                        StringRequest stringCityRequest = new StringRequest(Request.Method.GET, ConstantFields.getCityList(CountryId, String.valueOf(StateId)),
                                                                                responseCity -> {
                                                                                    pb_AddressLoader.setVisibility(View.GONE);
                                                                                    ArrayList<String> cityList;

                                                                                    if (ConstantDataSaver.mRetrieveSharedPreferenceString(activity, "setDefault") != null) {
                                                                                        if (ConstantDataSaver.mRetrieveSharedPreferenceString(activity, "setDefault").equals("true")) {
                                                                                            cityList = ConstantDataParser.getCityList(responseCity, true);
                                                                                        } else {
                                                                                            cityList = ConstantDataParser.getCityList(responseCity, true);
                                                                                        }
                                                                                    } else {
                                                                                        cityList = ConstantDataParser.getCityList(responseCity, true);
                                                                                    }

                                                                                    if (cityList != null) {
                                                                                        if (cityList.size() > 0) {

                                                                                            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>
                                                                                                    (activity, android.R.layout.simple_spinner_item, cityList);
                                                                                            spinnerArrayAdapter.setDropDownViewResource(android.R.layout
                                                                                                    .simple_spinner_dropdown_item);

                                                                                            s_CityList.setAdapter(spinnerArrayAdapter);
                                                                                            s_CityList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                                                                                @Override
                                                                                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                                                                                    CityName = cityList.get(position);
                                                                                                }

                                                                                                @Override
                                                                                                public void onNothingSelected(AdapterView<?> parent) {

                                                                                                }
                                                                                            });
                                                                                        }
                                                                                    }

                                                                                },
                                                                                error -> {
                                                                                    pb_AddressLoader.setVisibility(View.GONE);
                                                                                }) {

                                                                            @Override
                                                                            public Map<String, String> getHeaders() throws AuthFailureError {
                                                                                Map<String, String> params = new HashMap<>();
                                                                                params.put("Content-Type", "application/json; charset=utf-8");
                                                                                return params;
                                                                            }
                                                                        };
                                                                        stringCityRequest.setShouldCache(false);
                                                                        ApplicationContext.getInstance().addToRequestQueue(stringCityRequest, "City");
                                                                    }
                                                                }
                                                            }

                                                            @Override
                                                            public void onNothingSelected(AdapterView<?> arg0) {
                                                            }

                                                        });
                                                    } else {
                                                        List<String> noStates = new ArrayList<>();
                                                        noStates.add("--- None ---");

                                                        ArrayAdapter<String> adapter_StateN = new ArrayAdapter<>(activity, R.layout.rc_row_empty, R.id.atv_empty_message, noStates);
                                                        s_StateList.setAdapter(adapter_StateN);
                                                        StateCode = "0";
                                                        StateId = 0;
                                                    }
                                                } else {
                                                    List<String> noStates = new ArrayList<>();
                                                    noStates.add("--- None ---");

                                                    ArrayAdapter<String> adapter_StateN = new ArrayAdapter<>(activity, R.layout.rc_row_empty, R.id.atv_empty_message, noStates);
                                                    s_StateList.setAdapter(adapter_StateN);
                                                    StateCode = "0";
                                                    StateId = 0;
                                                }
                                            } else {
                                                List<String> noStates = new ArrayList<>();
                                                noStates.add("--- Please select country first ---");

                                                ArrayAdapter<String> adapter_StateN = new ArrayAdapter<>(activity, R.layout.rc_row_empty, R.id.atv_empty_message, noStates);
                                                s_StateList.setAdapter(adapter_StateN);
                                            }
                                        } else {

                                            List<String> noStates = new ArrayList<>();
                                            noStates.add("--- Please select country first ---");

                                            ArrayAdapter<String> adapter_StateN = new ArrayAdapter<>(activity, R.layout.rc_row_empty, R.id.atv_empty_message, noStates);
                                            s_StateList.setAdapter(adapter_StateN);
                                        }
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> adapterView) {

                                    }
                                });
                            }
                        }
                    }, error -> {
                pb_AddressLoader.setVisibility(View.GONE);
                try {
                    if (error.networkResponse != null) {
                        String responseBody = new String(error.networkResponse.data, "utf-8");
                        JSONObject jsonObject = new JSONObject(responseBody);
                        showToast(jsonObject.getString("message"));
                    } else {
                        showToast(getString(R.string.user_error_invalid_user_detail));
                    }
                } catch (Exception e) {
                    showToast(getString(R.string.user_error_invalid_user_detail));
                }
            })

            {

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<>();
                    params.put("Content-Type", "application/json; charset=utf-8");
                    return params;
                }
            };

            stringRequest.setShouldCache(false);
            ApplicationContext.getInstance().addToRequestQueue(stringRequest, "Country");
        }
    }

    private void setupBtn() {
        btn_SaveAddress.setOnClickListener(view -> {
            if (et_FirstName.getText().toString().length() > 0
                    && et_LastName.getText().toString().length() > 0
                    && et_MobileNumber.getText().toString().length() == 10
                    && et_StreetAddress.getText().toString().length() > 0
                    && !CityName.equals("Empty")
                    && et_ZipCode.getText().toString().length() > 0

                    && CountryId != null
                    && CountryId.length() > 0

                    && StateCode != null
                    && StateCode.length() > 0) {

                try {
                    JSONArray jsonArray = new JSONArray();

                    if (accountDetail != null) {
                        if (accountDetail.getAddressList() != null) {
                            if (accountDetail.getAddressList().size() > 0) {
                                for (int i = 0; i < accountDetail.getAddressList().size(); i++) {
                                    if (addressType.equals("edit")) {

                                        if (AddressId == accountDetail.getAddressList().get(i).getAddressId()) {

                                            JSONArray jsonStreetArrayEdit = new JSONArray();
                                            jsonStreetArrayEdit.put(et_StreetAddress.getText().toString());

                                            JSONObject jsonEditAddress = new JSONObject();
                                            jsonEditAddress.put("firstname", et_FirstName.getText().toString());
                                            jsonEditAddress.put("id", AddressId);
                                            jsonEditAddress.put("lastname", et_LastName.getText().toString());
                                            jsonEditAddress.put("telephone", et_MobileNumber.getText().toString());
                                            jsonEditAddress.put("street", jsonStreetArrayEdit);
                                            jsonEditAddress.put("city", CityName);
                                            jsonEditAddress.put("region_id", StateId);
                                            jsonEditAddress.put("region", StateCode);
                                            jsonEditAddress.put("country_id", CountryId);
                                            jsonEditAddress.put("postcode", et_ZipCode.getText().toString());

                                            if (cb_BillingAddress.isChecked()) {
                                                jsonEditAddress.put("default_billing", true);
                                            } else {
                                                if (accountDetail.getAddressList().get(i).isBillingAddress()) {
                                                    jsonEditAddress.put("default_billing", true);
                                                } else {
                                                    jsonEditAddress.put("default_billing", false);
                                                }
                                            }
                                            if (cb_ShippingAddress.isChecked()) {
                                                jsonEditAddress.put("default_shipping", true);
                                            } else {
                                                if (accountDetail.getAddressList().get(i).isBillingAddress()) {
                                                    jsonEditAddress.put("default_billing", true);
                                                } else {
                                                    jsonEditAddress.put("default_shipping", false);
                                                }
                                            }

                                            jsonArray.put(jsonEditAddress);

                                        } else {

                                            JSONArray jsonStreetArrayDefault = new JSONArray();
                                            jsonStreetArrayDefault.put(accountDetail.getAddressList().get(i).getStreet());

                                            JSONObject jsonAddress = new JSONObject();
                                            jsonAddress.put("firstname", accountDetail.getAddressList().get(i).getFirstName());
                                            jsonAddress.put("lastname", accountDetail.getAddressList().get(i).getLastName());
                                            jsonAddress.put("id", accountDetail.getAddressList().get(i).getAddressId());
                                            jsonAddress.put("telephone", accountDetail.getAddressList().get(i).getTelephone());
                                            jsonAddress.put("street", jsonStreetArrayDefault);
                                            jsonAddress.put("city", accountDetail.getAddressList().get(i).getCity());
                                            jsonAddress.put("region_id", accountDetail.getAddressList().get(i).getRegionId());
                                            jsonAddress.put("region", accountDetail.getAddressList().get(i).getRegionCode());
                                            jsonAddress.put("country_id", accountDetail.getAddressList().get(i).getCountryId());
                                            jsonAddress.put("postcode", accountDetail.getAddressList().get(i).getPost_code());

                                            if (!cb_BillingAddress.isChecked()) {
                                                if (accountDetail.getAddressList().get(i).isBillingAddress()) {
                                                    jsonAddress.put("default_billing", true);
                                                } else {
                                                    jsonAddress.put("default_billing", false);
                                                }
                                            } else {
                                                jsonAddress.put("default_billing", false);
                                            }
                                            if (!cb_ShippingAddress.isChecked()) {
                                                if (accountDetail.getAddressList().get(i).isShippingAddress()) {
                                                    jsonAddress.put("default_shipping", true);
                                                } else {
                                                    jsonAddress.put("default_shipping", false);
                                                }
                                            } else {
                                                jsonAddress.put("default_shipping", false);
                                            }

                                            jsonArray.put(jsonAddress);

                                        }
                                    } else {

                                        JSONArray jsonStreetArrayDefault = new JSONArray();
                                        jsonStreetArrayDefault.put(accountDetail.getAddressList().get(i).getStreet());

                                        JSONObject jsonAddress = new JSONObject();
                                        jsonAddress.put("firstname", accountDetail.getAddressList().get(i).getFirstName());
                                        jsonAddress.put("lastname", accountDetail.getAddressList().get(i).getLastName());
                                        jsonAddress.put("id", accountDetail.getAddressList().get(i).getAddressId());
                                        jsonAddress.put("telephone", accountDetail.getAddressList().get(i).getTelephone());
                                        jsonAddress.put("street", jsonStreetArrayDefault);
                                        jsonAddress.put("city", accountDetail.getAddressList().get(i).getCity());
                                        jsonAddress.put("region_id", accountDetail.getAddressList().get(i).getRegionId());
                                        jsonAddress.put("region", accountDetail.getAddressList().get(i).getRegionCode());
                                        jsonAddress.put("country_id", accountDetail.getAddressList().get(i).getCountryId());
                                        jsonAddress.put("postcode", accountDetail.getAddressList().get(i).getPost_code());

                                        if (!cb_BillingAddress.isChecked()) {
                                            if (accountDetail.getAddressList().get(i).isBillingAddress()) {
                                                jsonAddress.put("default_billing", true);
                                            } else {
                                                jsonAddress.put("default_billing", false);
                                            }
                                        } else {
                                            jsonAddress.put("default_billing", false);
                                        }
                                        if (!cb_ShippingAddress.isChecked()) {
                                            if (accountDetail.getAddressList().get(i).isShippingAddress()) {
                                                jsonAddress.put("default_shipping", true);
                                            } else {
                                                jsonAddress.put("default_shipping", false);
                                            }
                                        } else {
                                            jsonAddress.put("default_shipping", false);
                                        }

                                        jsonArray.put(jsonAddress);

                                    }
                                }
                            }
                        }
                    }

                    if (addressType.equals("New")) {

                        JSONArray jsonStreetArrayNew = new JSONArray();
                        jsonStreetArrayNew.put(et_StreetAddress.getText().toString());

                        JSONObject jsonNewAddress = new JSONObject();
                        jsonNewAddress.put("firstname", et_FirstName.getText().toString());
                        jsonNewAddress.put("lastname", et_LastName.getText().toString());
                        jsonNewAddress.put("telephone", et_MobileNumber.getText().toString());
                        jsonNewAddress.put("street", jsonStreetArrayNew);
                        jsonNewAddress.put("city", CityName);
                        jsonNewAddress.put("region_id", StateId);
                        jsonNewAddress.put("region", StateCode);
                        jsonNewAddress.put("country_id", CountryId);
                        jsonNewAddress.put("postcode", et_ZipCode.getText().toString());

                        if (cb_BillingAddress.isChecked()) {
                            jsonNewAddress.put("default_billing", true);
                        } else {
                            jsonNewAddress.put("default_billing", false);
                        }
                        if (cb_ShippingAddress.isChecked()) {
                            jsonNewAddress.put("default_shipping", true);
                        } else {
                            jsonNewAddress.put("default_shipping", false);
                        }

                        jsonArray.put(jsonNewAddress);
                    }

                    JSONObject jsonObjectCustomer = new JSONObject();
                    jsonObjectCustomer.put("firstname", accountDetail.getFirstName());
                    jsonObjectCustomer.put("lastname", accountDetail.getLastName());
                    jsonObjectCustomer.put("email", accountDetail.getEmailId());
                    jsonObjectCustomer.put("websiteId", 0);
                    jsonObjectCustomer.put("addresses", jsonArray);

                    JSONObject jsonFinalObject = new JSONObject();
                    jsonFinalObject.put("customer", jsonObjectCustomer);

                    new AsyncTaskForPost().execute(jsonFinalObject.toString());

                    Log.e("onClick: ", jsonFinalObject.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("onClick: ", e.toString());
                }

            }

            if (et_FirstName.getText().toString().equals("")) {
                atv_FirstNameError.setVisibility(View.VISIBLE);
            } else {
                atv_FirstNameError.setVisibility(View.GONE);
            }
            if (et_LastName.getText().toString().equals("")) {
                atv_LastNameError.setVisibility(View.VISIBLE);
            } else {
                atv_LastNameError.setVisibility(View.GONE);
            }
            if (et_MobileNumber.getText().toString().equals("")) {
                atv_MobileNumberError.setVisibility(View.VISIBLE);
            } else {
                if (et_MobileNumber.getText().toString().length() == 10) {
                    atv_MobileNumberError.setVisibility(View.GONE);
                } else {
                    atv_MobileNumberError.setVisibility(View.VISIBLE);
                }
            }
            if (et_StreetAddress.getText().toString().equals("")) {
                atv_StreetAddressError.setVisibility(View.VISIBLE);
            } else {
                atv_StreetAddressError.setVisibility(View.GONE);
            }
            if (CityName.equals("Empty")) {
                atv_CityError.setVisibility(View.VISIBLE);
            } else {
                atv_CityError.setVisibility(View.GONE);
            }

            if (et_ZipCode.getText().toString().equals("")) {
                atv_ZipCodeError.setVisibility(View.VISIBLE);
            } else {
                atv_ZipCodeError.setVisibility(View.GONE);
            }

            if (CountryId != null) {
                if (CountryId.length() > 0) {
                    atv_CountryError.setVisibility(View.GONE);
                } else {
                    atv_CountryError.setVisibility(View.VISIBLE);
                }
            } else {
                atv_CountryError.setVisibility(View.VISIBLE);
            }

            if (StateCode != null) {
                if (StateCode.length() > 0) {
                    atv_StateError.setVisibility(View.GONE);
                } else {
                    atv_StateError.setVisibility(View.VISIBLE);
                }
            } else {
                atv_StateError.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        // safety check
        if (getDialog() == null)
            return;

        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.WRAP_CONTENT;

        if (getDialog().getWindow() != null)
            getDialog().getWindow().setLayout(width, height);
    }

    private void showToast(String message) {
        ConstantFields.CustomToast(message, activity);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(AppLanguageSupport.onAttach(context));
        activity = (Activity) context;
        menuHandler = (MenuHandler) context;

    }

    @SuppressLint("StaticFieldLeak")
    private class AsyncTaskForPost extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pb_AddressLoader.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                URL url = new URL(ConstantFields.getCustomerDetail(ConstantFields.currentLanguage().equals("en")));
                Log.d("POST url", url.toString());
                HttpURLConnection mUrlConnection = (HttpURLConnection) url.openConnection();
                mUrlConnection.setRequestMethod("PUT");
                mUrlConnection.setRequestProperty("Content-Type", "application/json");
                mUrlConnection.setRequestProperty("Authorization", "Bearer " + ConstantDataSaver.
                        mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mTokenTag));

                mUrlConnection.setDoInput(true);
                mUrlConnection.setDoOutput(true);
                mUrlConnection.setReadTimeout(15000);
                OutputStream mOutputStream = mUrlConnection.getOutputStream();

                BufferedWriter mBufferedWriter = new BufferedWriter(new OutputStreamWriter(mOutputStream, "UTF-8"));
                mBufferedWriter.write(params[0]);
                mBufferedWriter.close();
                mOutputStream.close();

                String mResponse;
                if (mUrlConnection.getResponseCode() == 200) {
                    mResponse = inputStreamToStringConversion(new BufferedReader(new InputStreamReader(mUrlConnection.getInputStream())));
                } else {
                    mResponse = "Error";
                }
                Log.e("doInBackground: ", mResponse);
                return mResponse;
            } catch (Exception e) {
                Log.e("doInBackground: ", e.toString());
                return null;
            }
        }

        @Override
        protected void onPostExecute(String s) {
            pb_AddressLoader.setVisibility(View.GONE);
            dismiss();
            menuHandler.reloadAddressBook();
        }

        @Nullable
        String inputStreamToStringConversion(BufferedReader bufferedReader) {
            try {
                String line;
                StringBuilder result = new StringBuilder();
                while ((line = bufferedReader.readLine()) != null) {
                    result.append(line);
                }
                return result.toString();
            } catch (Exception e) {
                return null;
            }
        }
    }

    private void fontSetup() {
        fontSetup(et_FirstName, Constant.LIGHT, Constant.C_EDIT_TEXT);
        fontSetup(et_LastName, Constant.LIGHT, Constant.C_EDIT_TEXT);
        fontSetup(et_MobileNumber, Constant.LIGHT, Constant.C_EDIT_TEXT);
        fontSetup(et_StreetAddress, Constant.LIGHT, Constant.C_EDIT_TEXT);
        fontSetup(et_ZipCode, Constant.LIGHT, Constant.C_EDIT_TEXT);

        fontSetup(atv_FirstNameError, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_LastNameError, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_MobileNumberError, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_StreetAddressError, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_CityError, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_StateError, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_CountryError, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_ZipCodeError, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_AddressTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_ContactTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_FirstNameTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_LastNameTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_MobileNoTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_AddressInfoTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_StreetAddressTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_CityTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_StateTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_CountryTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_ZipCodeTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);

        fontSetup(cb_BillingAddress, Constant.LIGHT, Constant.C_CHECKBOX);
        fontSetup(cb_ShippingAddress, Constant.LIGHT, Constant.C_CHECKBOX);

        fontSetup(btn_SaveAddress, Constant.LIGHT, Constant.C_BUTTON);
    }

    private void fontSetup(Object o_SetFont, int type, int which) {
        Constant.SetFontStyle(o_SetFont, type, which);
    }
}
