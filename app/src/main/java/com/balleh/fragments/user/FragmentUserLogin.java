package com.balleh.fragments.user;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import com.balleh.R;
import com.balleh.additional.AppLanguageSupport;
import com.balleh.additional.ConstantFields;
import com.balleh.additional.CustomCrashReport;
import com.balleh.additional.instentTransfer.User;

import org.json.JSONObject;

/**
 * Created by Sasikumar on 3/23/2018.
 * User Login
 */

public class FragmentUserLogin extends Fragment implements View.OnClickListener {

    private View v_LoginContainer;
    private EditText et_Email, et_Password;
    private User mUserLoginInstantTransfer;
    private Activity activity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(AppLanguageSupport.onAttach(context));
        mUserLoginInstantTransfer = (User) context;
        activity = getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v_LoginContainer = inflater.inflate(R.layout.fragment_user_login, container, false);

        String TAG = FragmentUserLogin.class.getSimpleName();
        if (!(Thread.getDefaultUncaughtExceptionHandler() instanceof CustomCrashReport)) {
            Thread.setDefaultUncaughtExceptionHandler(new CustomCrashReport(TAG, getActivity()));
        }

        load();
        return v_LoginContainer;
    }

    private void load() {
        Button btn_SignIn = v_LoginContainer.findViewById(R.id.btn_sign_in);
        Button btn_SignUp = v_LoginContainer.findViewById(R.id.btn_sign_up);
        AppCompatTextView atv_ForgetPassword = v_LoginContainer.findViewById(R.id.atv_forget_password);
        CheckBox cb_ShowPwd = v_LoginContainer.findViewById(R.id.cb_show_pwd_sign_in);
        et_Email = v_LoginContainer.findViewById(R.id.et_email_login);
        et_Password = v_LoginContainer.findViewById(R.id.et_password_login);

        btn_SignIn.setOnClickListener(view -> {

            if (et_Email.getEditableText().toString().length() > 0
                    && et_Password.getEditableText().toString().length() > 0
                    && et_Password.getEditableText().toString().length() > 7
                    && ConstantFields.isEmailValidator(et_Email.getEditableText().toString())) {

                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("username", et_Email.getEditableText().toString());
                    jsonObject.put("password", et_Password.getEditableText().toString());
                    //mUserLoginInstantTransfer.SignIn(jsonObject);
                } catch (Exception e) {
                    showMessage(getString(R.string.common_error));
                }
            }

            if (et_Email.getText().toString().isEmpty()) {
                showMessage(getString(R.string.user_error_email));
            } else {
                if (!ConstantFields.isEmailValidator(et_Email.getText().toString())) {
                    showMessage(getString(R.string.user_error_invalid_email));
                } else {
                    if (et_Password.getText().toString().isEmpty()) {
                        showMessage(getString(R.string.user_error_password));
                    } else {
                        if (et_Password.getText().toString().length() < 8) {
                            showMessage(getString(R.string.user_error_password_length));
                        }
                    }
                }
            }
        });

        cb_ShowPwd.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            if (!isChecked) {
                et_Password.setTransformationMethod(PasswordTransformationMethod.getInstance());
            } else {
                et_Password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            }
        });

        btn_SignUp.setOnClickListener(this);
        atv_ForgetPassword.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.btn_sign_up:
               // mUserLoginInstantTransfer.loadSignUp();
                break;
            case R.id.atv_forget_password:
                //mUserLoginInstantTransfer.loadForgetPassword();
                break;
        }
    }

    private void showMessage(String message) {
        ConstantFields.CustomToast(message, activity);
    }

}
